/***************************************************************************
                          roster-widget.cpp -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "chat-window.h"
#include "roster-delegate.h"
#include "roster-filter.h"
#include "roster-groupfilter.h"
#include "roster-item.h"
#include "roster-widget.h"
#include "status.h"


#include <TelepathyQt/Account>
#include <TelepathyQt/Contact>
#include <TelepathyQt/ContactManager>
#include <TelepathyQt/PendingConnection>
#include <TelepathyQt/PendingContacts>
#include <TelepathyQt/PendingOperation>
#include <TelepathyQt/PendingReady>
#include <TelepathyQt/Types>

#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMessageBox>
#include <QPushButton>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QTreeView>
#include <QVBoxLayout>

using namespace Tp;

RosterWidget::RosterWidget(Tp::AccountPtr account, QWidget *parent)
    : QWidget(parent),
      mAccount(account),
      mSearchEnabled(false),
      mSettings(new QSettings())
{
    mList2 = new QStandardItemModel();

    createActions();
    setupGui();
}

RosterWidget::~RosterWidget()
{
}

void RosterWidget::setConnection(AccountPtr account/*const ConnectionPtr &conn*/)
{
    //if (mConn) {
    //    unsetConnection();
    //}

    mConn = account->connection();
    qDebug() << mConn->contactManager();
    connect(mConn->contactManager().data(),
            SIGNAL(presencePublicationRequested(const Tp::Contacts &)),
            SLOT(onPresencePublicationRequested(const Tp::Contacts &)));
    // TODO listen to allKnownContactsChanged

    connect(mConn->contactManager().data(),
            SIGNAL(stateChanged(Tp::ContactListState)),
            SLOT(onContactManagerStateChanged(Tp::ContactListState)));
    onContactManagerStateChanged(mConn->contactManager()->state());

    // Set avatar
    onAvatarChanged(account->avatar());
}

void RosterWidget::unsetConnection()
{
    //while (mList->topLevelItemCount/*count*/() > 0) {
    //    RosterItem *item = (RosterItem *) mList->/*takeItem*/takeTopLevelItem(0);
    //    delete item;
    //}
    //mConn.reset();
    updateActions();
    mAddContactAction->setEnabled(false);
}

void RosterWidget::createActions()
{
    mAuthAction = new QAction(QLatin1String("Authorize Contact"), this);
    mAuthAction->setEnabled(false);
    connect(mAuthAction,
            SIGNAL(triggered(bool)),
            SLOT(onAuthActionTriggered(bool)));
    mDenyAction = new QAction(QLatin1String("Deny Contact"), this);
    mDenyAction->setEnabled(false);
    connect(mDenyAction,
            SIGNAL(triggered(bool)),
            SLOT(onDenyActionTriggered(bool)));
    mRemoveAction = new QAction(QLatin1String("Remove Contact"), this);
    mRemoveAction->setEnabled(false);
    connect(mRemoveAction,
            SIGNAL(triggered(bool)),
            SLOT(onRemoveActionTriggered(bool)));
    mBlockAction = new QAction(QLatin1String("Block Contact"), this);
    mBlockAction->setEnabled(false);
    mBlockAction->setCheckable(true);
    connect(mBlockAction,
            SIGNAL(triggered(bool)),
            SLOT(onBlockActionTriggered(bool)));
}

void RosterWidget::setupGui()
{
    ui = Ui::mContactListView();
    ui.setupUi(this);

    mFilteredList = new RosterFilter();
    mFilteredList->setSourceModel(mList2);
    mFilteredList->sort(0);
    mFilteredList->setDynamicSortFilter(true);
    mFilteredList->setFilterCaseSensitivity(Qt::CaseInsensitive);

    mGroupFilteredList = new RosterGroupFilter();
    mGroupFilteredList->setSourceModel(mFilteredList);
    mGroupFilteredList->setDynamicSortFilter(true);

    connect(mList2,
        SIGNAL(dataChanged(const QModelIndex &, const QModelIndex &)),
        SLOT(onRosterItemChanged(const QModelIndex &, const QModelIndex &)));

    connect(ui.mContactList,
        SIGNAL(collapsed(const QModelIndex &)),
        SLOT(onGroupCollapsed(const QModelIndex &)));

    connect(ui.mContactList,
        SIGNAL(expanded(const QModelIndex &)),
        SLOT(onGroupExpanded(const QModelIndex &)));

    connect(mGroupFilteredList,
        SIGNAL(layoutChanged()),
        SLOT(onGroupLayoutChanged()));

    ui.mContactList->setModel(mGroupFilteredList);
    ui.mContactList->setItemDelegate(new RosterDelegate(ui.mContactList, mList2, mFilteredList, mGroupFilteredList));
    ui.mContactList->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ui.mContactList->setContextMenuPolicy(Qt::ActionsContextMenu);
    ui.mContactList->addAction(mAuthAction);
    ui.mContactList->addAction(mDenyAction);
    ui.mContactList->addAction(mRemoveAction);
    ui.mContactList->addAction(mBlockAction);

    connect(ui.mContactList->selectionModel(),
            SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection & )),
            SLOT(onItemSelectionChanged()));

    connect(ui.mContactList,
            SIGNAL(doubleClicked(const QModelIndex &)),
            SLOT(onContactListItemClicked(const QModelIndex &)));

    mStatusMenu = Status::statusMenu();
    ui.mStatusButton->setMenu(mStatusMenu);
    connect( ui.mStatusButton, SIGNAL(clicked()),
             ui.mStatusButton, SLOT  (showMenu()) );
    onCurrentPresenceChanged(mAccount->currentPresence());

    mAddDlg = new QDialog(this);
    mAddDlg->setWindowTitle(QLatin1String("Add Contact"));
    QVBoxLayout *addDlgVBox = new QVBoxLayout;

    QHBoxLayout *addDlgEntryHBox = new QHBoxLayout;
    QLabel *label = new QLabel(QLatin1String("Username"));
    addDlgEntryHBox->addWidget(label);
    mAddDlgEdt = new QLineEdit();
    addDlgEntryHBox->addWidget(mAddDlgEdt);
    addDlgVBox->addLayout(addDlgEntryHBox);

    QDialogButtonBox *addDlgBtnBox = new QDialogButtonBox(
            QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal);
    connect(addDlgBtnBox, SIGNAL(accepted()), mAddDlg, SLOT(accept()));
    connect(addDlgBtnBox, SIGNAL(rejected()), mAddDlg, SLOT(reject()));
    addDlgVBox->addWidget(addDlgBtnBox);

    mAddDlg->setLayout(addDlgVBox);

    connect(mAccount.data(),
            SIGNAL(avatarChanged(const Tp::Avatar &)),
            SLOT(onAvatarChanged(const Tp::Avatar &)));

    connect(mAccount.data(),
            SIGNAL(currentPresenceChanged(const Tp::Presence &)),
            SLOT(onCurrentPresenceChanged(const Tp::Presence &)));

    ui.mSearchFrame->setVisible(false);
    ui.mSearchIcon->setPixmap(QIcon::fromTheme("edit-find-user").pixmap(16,16));
    // Connect the search edit line with slot for searching
    ui.mSearchEdit->setPlaceholderText("Search in contact list...");
    connect( ui.mSearchEdit,
        SIGNAL(   textChanged(const QString &) ),
        SLOT  ( searchContact(const QString &) ) );

    ui.mContactsButton->setIcon(QIcon::fromTheme("user-identity").pixmap(16,16));

    QMenu* mContactsMenu = new QMenu();


    mSearchContactAction = new QAction(QIcon::fromTheme("edit-find-user"), "Search contact",        this);
    mAddContactAction    = new QAction(QIcon::fromTheme("list-add-user"),  "Add contact",           this);
    mOfflineContactAction= new QAction("Show Offline contacts", this);
    mEmptyGroupsAction   = new QAction("Show Empty groups", this);

    mSearchContactAction->setShortcuts(QKeySequence::Find);
    mSearchContactAction->setCheckable(true);
    mAddContactAction->setEnabled(false);
    mOfflineContactAction->setCheckable(true);
    mEmptyGroupsAction->setCheckable(true);

    mOfflineContactAction->setChecked(mSettings->value("roster-widget/showOffLineContacts", true).toBool());
    mEmptyGroupsAction->setChecked(mSettings->value("roster-widget/showEmptyGroups", true).toBool());
    mSearchContactAction->setChecked(mSettings->value("roster-widget/showSearchWidget", true).toBool());

    mContactsMenu->addAction(mSearchContactAction);
    mContactsMenu->addAction(mAddContactAction);
    mContactsMenu->addSeparator();
    mContactsMenu->addAction(mOfflineContactAction);
    mContactsMenu->addAction(mEmptyGroupsAction);

    connect( mSearchContactAction, SIGNAL(          triggered(bool)      ),
                                   SLOT  (     onToggleSearch()          ) );
    connect( mAddContactAction,    SIGNAL(          triggered(bool)      ),
                                   SLOT  ( onAddButtonClicked()          ) );
    connect( mOfflineContactAction,SIGNAL(          triggered(bool)      ),
                                   SLOT  ( onToggleOfflineContacts()     ) );
    connect( mEmptyGroupsAction,   SIGNAL(          triggered(bool)      ),
                                   SLOT  ( onToggleEmptyGroups()         ) );

    ui.mContactsButton->setMenu(mContactsMenu);

    // Update filtering settings
    onToggleSearch();
    onToggleOfflineContacts();
    onToggleEmptyGroups();
}

void RosterWidget::searchContact(const QString &searchFor)
{
  QString searchExpression;
  const QString regexpMagic( "regexp:" );

  // If the search string starts with "regexp:", allow searching with a regexp :)
  if( searchFor.startsWith( regexpMagic ) )
  {
    searchExpression = searchFor.mid( regexpMagic.length() );

    // Colorize the search bar when using regexps, according to if they're valid or invalid
    if( QRegExp( searchExpression ).isValid() )
    {
      ui.mSearchEdit->setStyleSheet( "background-color: #bbffbb" );
    }
    else
    {
      ui.mSearchEdit->setStyleSheet( "background-color: #ffbbbb" );
    }
  }
  else
  {
    // Remove trailing spaces and other unneeded (invisible) characters, then escape the searchstring
    searchExpression = QRegExp::escape( searchFor.simplified() );
    ui.mSearchEdit->setStyleSheet( "" );
  }

  bool isSearching = searchExpression.length() != 0;
  //Hide empty groups when searching
  mGroupFilteredList->setShowEmptyGroups(isSearching?false:mEmptyGroupsAction->isChecked());
  //Show offline contacts
  mFilteredList->setShowOfflineContacts(isSearching?true:mOfflineContactAction->isChecked());

  mFilteredList->setFilterRegExp( searchExpression );
  mFilteredList->invalidate();
}

void RosterWidget::onContactListItemClicked(const QModelIndex &index)
{
  if(index.data(RosterItem::TypeRole) == RosterItem::ContactType)
  {
    qDebug() << "Opening chat for" << index.data(RosterItem::IdRole).toString();
    mAccount->ensureTextChat(index.data(RosterItem::IdRole).toString());
    //connect((const QObject*)account->ensureTextChat(contact), SIGNAL(channelRequestCreated(const Tp::ChannelRequestPtr &)),
    //        SLOT(channelRequestCreated(const Tp::ChannelRequestPtr &)));
  }
}

RosterItem *RosterWidget::createItemForContact(const ContactPtr &contact,
        bool &exists)
{
    RosterItem *item;
    exists = false;

    foreach(QObject* object, mList2->children())
    {
      item = dynamic_cast<RosterItem*>(object);
      if (item->contact() == contact) {
        exists = true;
        return item;
      }
    }

    return new RosterItem(contact, mList2);
}

void RosterWidget::onContactManagerStateChanged(ContactListState state)
{
    if (state == ContactListStateSuccess) {
        qDebug() << "Loading contacts";
        RosterItem *item;
        bool exists;
        foreach (const ContactPtr &contact, mConn->contactManager()->allKnownContacts()) {
            exists = false;
            item = createItemForContact(contact, exists);
            if (!exists) {
                connect(item, SIGNAL(changed()), SLOT(updateActions()));
            }
        }

        mAddContactAction->setEnabled(true);
    }
}

void RosterWidget::onPresencePublicationRequested(const Contacts &contacts)
{
    qDebug() << "Presence publication requested";
    RosterItem *item;
    bool exists;
    foreach (const ContactPtr &contact, contacts) {
        exists = false;
        item = createItemForContact(contact, exists);
        if (!exists) {
            connect(item, SIGNAL(changed()), SLOT(updateActions()));
        }
    }
}

void RosterWidget::onItemSelectionChanged()
{
  updateActions();
}

void RosterWidget::onAddButtonClicked()
{
    mAddDlgEdt->clear();
    int ret = mAddDlg->exec();
    if (ret == QDialog::Rejected) {
        return;
    }

    QString username = mAddDlgEdt->text();
    PendingContacts *pcontacts = mConn->contactManager()->contactsForIdentifiers(
            QStringList() << username);
    connect(pcontacts,
            SIGNAL(finished(Tp::PendingOperation *)),
            SLOT(onContactRetrieved(Tp::PendingOperation *)));
}

void RosterWidget::onAuthActionTriggered(bool checked)
{
    Q_UNUSED(checked);

    QModelIndexList selectedItems = ui.mContactList->selectionModel()->selectedIndexes();
    if (selectedItems.isEmpty()) {
        return;
    }

    Q_ASSERT(selectedItems.size() == 1);
    RosterItem *item = dynamic_cast<RosterItem*>(mList2->itemFromIndex(mFilteredList->mapToSource(mGroupFilteredList->mapToSource(selectedItems.first()))));

    if (item->contact()->publishState() != Contact::PresenceStateYes) {
        item->contact()->authorizePresencePublication();
    }
}

void RosterWidget::onDenyActionTriggered(bool checked)
{
    qDebug() << "DENY";
    Q_UNUSED(checked);

    QModelIndexList selectedItems = ui.mContactList->selectionModel()->selectedIndexes();
    if (selectedItems.isEmpty()) {
        return;
    }

    Q_ASSERT(selectedItems.size() == 1);
    RosterItem *item = dynamic_cast<RosterItem*>(mList2->itemFromIndex(mFilteredList->mapToSource(mGroupFilteredList->mapToSource(selectedItems.first()))));

    if (item->contact()->publishState() != Contact::PresenceStateNo) {
        // The contact can't see my presence
        item->contact()->removePresencePublication();
    }
}

void RosterWidget::onRemoveActionTriggered(bool checked)
{
    Q_UNUSED(checked);

    QModelIndexList selectedItems = ui.mContactList->selectionModel()->selectedIndexes();
    if (selectedItems.isEmpty()) {
        return;
    }

    Q_ASSERT(selectedItems.size() == 1);
    RosterItem *item = dynamic_cast<RosterItem*>(mList2->itemFromIndex(mFilteredList->mapToSource(mGroupFilteredList->mapToSource(selectedItems.first()))));
    if (item->contact()->subscriptionState() != Contact::PresenceStateNo) {
        // The contact can't see my presence and I can't see his/her presence
        item->contact()->removePresencePublication();
        item->contact()->removePresenceSubscription();
    }
}

void RosterWidget::onBlockActionTriggered(bool checked)
{
    QModelIndexList selectedItems = ui.mContactList->selectionModel()->selectedIndexes();
    if (selectedItems.isEmpty()) {
        return;
    }

    Q_ASSERT(selectedItems.size() == 1);
    RosterItem *item = dynamic_cast<RosterItem*>(mList2->itemFromIndex(mFilteredList->mapToSource(mGroupFilteredList->mapToSource(selectedItems.first()))));
    if (checked) {
        item->contact()->block();
    } else {
        item->contact()->unblock();
    }
}

void RosterWidget::onContactRetrieved(Tp::PendingOperation *op)
{
    PendingContacts *pcontacts = qobject_cast<PendingContacts *>(op);
    QList<ContactPtr> contacts = pcontacts->contacts();
    Q_ASSERT(pcontacts->identifiers().size() == 1);
    QString username = pcontacts->identifiers().first();
    if (contacts.size() != 1 || !contacts.first()) {
        QMessageBox msgBox;
        msgBox.setText(QString(QLatin1String("Unable to add contact \"%1\"")).arg(username));
        msgBox.exec();
        return;
    }

    ContactPtr contact = contacts.first();
    qDebug() << "Request presence subscription for contact" << username;
    bool exists = false;
    RosterItem *item = createItemForContact(contact, exists);
    if (!exists) {
        connect(item, SIGNAL(changed()), SLOT(updateActions()));
    }
    contact->requestPresenceSubscription();
}

void RosterWidget::updateActions()
{
    QModelIndexList selectedItems = ui.mContactList->selectionModel()->selectedIndexes();
    //QList<QStandardItem *> selectedItems = list->selectedItems();
    if (selectedItems.isEmpty()) {
        mAuthAction->setEnabled(false);
        mDenyAction->setEnabled(false);
        mRemoveAction->setEnabled(false);
        mBlockAction->setEnabled(false);
        updateActions(0);
        return;
    }
    Q_ASSERT(selectedItems.size() == 1);

    QModelIndex index = selectedItems.first();
    QStandardItem* stditem = mList2->itemFromIndex(mFilteredList->mapToSource(mGroupFilteredList->mapToSource(index)));


    RosterItem *item = dynamic_cast<RosterItem*>(stditem);
    if(item)
    {
      ContactPtr contact = item->contact();

      ContactManagerPtr manager = contact->manager();
      qDebug() << "Contact" << contact->id() << "selected";
      qDebug() << " subscription state:" << contact->subscriptionState();
      qDebug() << " publish state     :" << contact->publishState();
      qDebug() << " blocked           :" << contact->isBlocked();

      if (manager->canAuthorizePresencePublication() &&
          contact->publishState() == Contact::PresenceStateAsk) {
        mAuthAction->setEnabled(true);
      } else {
        mAuthAction->setEnabled(false);
      }

      if (manager->canRemovePresencePublication() &&
          contact->publishState() != Contact::PresenceStateNo) {
        mDenyAction->setEnabled(true);
      } else {
        mDenyAction->setEnabled(false);
      }

      if (manager->canRemovePresenceSubscription() &&
          contact->subscriptionState() != Contact::PresenceStateNo) {
        mRemoveAction->setEnabled(true);
      } else {
        mRemoveAction->setEnabled(false);
      }

      if (manager->canBlockContacts() &&
          contact->publishState() == Contact::PresenceStateYes) {
        mBlockAction->setEnabled(true);
      } else {
        mBlockAction->setEnabled(false);
      }

      mBlockAction->setChecked(contact->isBlocked());
      }
    else
    {
      mAuthAction->setEnabled(false);
      mDenyAction->setEnabled(false);
      mRemoveAction->setEnabled(false);
      mBlockAction->setEnabled(false);
      updateActions(0);
    }

    updateActions(item);
}

void RosterWidget::onAvatarChanged(const Tp::Avatar &avatar)
{
  qDebug() << "Avatar changed";
  QPixmap pixmap = QPixmap();
  if(!pixmap.loadFromData(avatar.avatarData))
  {
    QString filepath = QCoreApplication::applicationDirPath() + "/../share/apps/kmess-telepathy/pics/unknown.png";
    pixmap = QPixmap(filepath);
    qDebug() << "Roster: No avatar found, showing default" << filepath;
  }
  ui.mAvatar->setPixmap(pixmap);
}

void RosterWidget::onCurrentPresenceChanged(const Tp::Presence &presence)
{
  ui.mStatusButton->setText(presence.status());
  ui.mStatusButton->setIcon(Status::statusMap()->value(presence.status()));
}

void RosterWidget::onRosterItemChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
  if(!topLeft.parent().isValid())
  {
    Q_ASSERT(topLeft == bottomRight);
    if(ui.mContactList->isExpanded(topLeft) != topLeft.data(RosterItem::ExpandedRole).toBool())
    {
      ui.mContactList->setExpanded(mGroupFilteredList->mapFromSource(mFilteredList->mapFromSource(topLeft)), true);
    }
  }
}

void RosterWidget::onGroupCollapsed(const QModelIndex & index)
{
  if(!mSearchEnabled)
  {
    mList2->itemFromIndex(
        mFilteredList->mapToSource(
          mGroupFilteredList->mapToSource(index)))->setData(QVariant(false), RosterItem::ExpandedRole);
  }
}

void RosterWidget::onGroupExpanded(const QModelIndex & index)
{
  if(!mSearchEnabled)
  {
    mList2->itemFromIndex(
        mFilteredList->mapToSource(
          mGroupFilteredList->mapToSource(index)))->setData(QVariant(true), RosterItem::ExpandedRole);
  }
}

void RosterWidget::onGroupLayoutChanged()
{
  bool searchEnabled = ui.mSearchEdit->text().length() != 0;
  if(searchEnabled == true)
  {
    mSearchEnabled = true;
    // Expand all groups
    ui.mContactList->expandAll();
  }
  else if(searchEnabled == false && mSearchEnabled == true)
  {
    // Exited from a search, restore original group collapsing
    mSearchEnabled = false;
    qDebug() << "restoring group collapsing";
    for(int i = 0 ; i < mList2->invisibleRootItem()->rowCount() ;i++)
    {
      QStandardItem* item = mList2->invisibleRootItem()->child(i, 0);
      qDebug() << item->text();
      ui.mContactList->setExpanded(
          mGroupFilteredList->mapFromSource(
            mFilteredList->mapFromSource(mList2->indexFromItem(item))),
          item->data(RosterItem::ExpandedRole).toBool());
    }
  }
}

void RosterWidget::onToggleSearch()
{
  bool visible = mSearchContactAction->isChecked();
  ui.mSearchFrame->setVisible(visible);
  mSearchContactAction->setChecked(visible);
  ui.mSearchEdit->setFocus();
  mSettings->setValue("roster-widget/showSearchWidget", visible);
}

void RosterWidget::onToggleOfflineContacts()
{
  bool show = mOfflineContactAction->isChecked();
  mFilteredList->setShowOfflineContacts(show);
  // Also invalidate group filter
  mGroupFilteredList->invalidate();
  mSettings->setValue("roster-widget/showOffLineContacts", show);
}

void RosterWidget::onToggleEmptyGroups()
{
  bool show = mEmptyGroupsAction->isChecked();
  mGroupFilteredList->setShowEmptyGroups(show);
  mSettings->setValue("roster-widget/showEmptyGroups", show);
}
