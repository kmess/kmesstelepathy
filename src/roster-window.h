/***************************************************************************
                          roster-window.h -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _KMESS_ROSTER_WINDOW_H_
#define _KMESS_ROSTER_WINDOW_H_

#include <QMainWindow>

#include <TelepathyQt/Account>
#include <TelepathyQt/Types>
#include <TelepathyQt/ReceivedMessage>
#include <TelepathyQt/TextChannel>

namespace Tp {
class PendingOperation;
}

class QStandardItemModel;

class KMessApplication;
class InitialView;
class RosterWidget;

class RosterWindow : public QMainWindow
{
    Q_OBJECT

public:
    RosterWindow(Tp::AccountPtr account, KMessApplication *app, QWidget *parent = 0);
    virtual ~RosterWindow();

private slots:
    /// User requested to change status
    void onStatusChange(QAction *action);
    /// Setup UI
    void setupGui();
    /// Account connection has changed
    void onConnectionStatusChanged(Tp::AccountPtr account, Tp::ConnectionStatus status);

private:
    Tp::AccountPtr mAccount;
    RosterWidget *mRoster;
    KMessApplication* mApplication;
};

#endif
