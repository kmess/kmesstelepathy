/***************************************************************************
                          kmess-application.cpp -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "account-item.h"
#include "chat-window.h"
#include "kmess-application.h"
#include "roster-window.h"
#include "status.h"

#include <TelepathyQt/AccountFactory>
#include <TelepathyQt/AccountManager>
#include <TelepathyQt/ChannelClassSpec>
#include <TelepathyQt/ChannelFactory>
#include <TelepathyQt/ClientRegistrar>
#include <TelepathyQt/ConnectionFactory>
#include <TelepathyQt/ContactFactory>
#include <TelepathyQt/DBusProxy>
#include <TelepathyQt/PendingReady>

#include <QDebug>
#include <QWeakPointer>
#include <QStandardItemModel>

inline Tp::ChannelClassSpecList channelClassList()
{
    return Tp::ChannelClassSpecList() << Tp::ChannelClassSpec::textChat()
                                      << Tp::ChannelClassSpec::unnamedTextChat();
                                      //<< Tp::ChannelClassSpec::textChatroom();
}

KMessApplication::KMessApplication(int &argc, char **argv)
  : QApplication(argc, argv),
    AbstractClientHandler(channelClassList()),
    mAccountFeatures(Tp::Features() << Tp::Account::FeatureCore << Tp::Account::FeatureAvatar),
    mAccounts(new QStandardItemModel()),
    mChats()
{
    QIcon::setThemeName("oxygen");
    QCoreApplication::setOrganizationName("KMess");
    QCoreApplication::setOrganizationDomain("kmess.org");
    QCoreApplication::setApplicationName("KMess Telepathy");

    mAccountManager = Tp::AccountManager::create(Tp::AccountFactory::create(QDBusConnection::sessionBus(),
                mAccountFeatures));


    mChannelFactory = Tp::ChannelFactory::create(
            QDBusConnection::sessionBus());
    mConnectionFactory = Tp::ConnectionFactory::create(
            QDBusConnection::sessionBus(), Tp::Connection::FeatureConnected |
                Tp::Connection::FeatureRoster | Tp::Connection::FeatureRosterGroups);
    mContactFactory = Tp::ContactFactory::create(
            Tp::Contact::FeatureAlias | Tp::Contact::FeatureSimplePresence | Tp::Contact::FeatureAvatarData );

    if(argc >= 1)
    {
      mAccountName = QLatin1String(argv[1]);
    }


   Tp::Features textFeatures = Tp::Features() << Tp::TextChannel::FeatureMessageQueue;
                                              //<< Tp::TextChannel::FeatureMessageSentSignal
                                              //<< Tp::TextChannel::FeatureChatState
                                              //<< Tp::TextChannel::FeatureMessageCapabilities;

    mChannelFactory->addFeaturesForTextChats(textFeatures);
}
/**
* Handle channel request
*
* When a text channel is requested, check whether a chat already exists.
* Create a new chat when the targetId is not known by #mChats or the stored pointer is invalid.
*
**/
void KMessApplication::handleChannels(const Tp::MethodInvocationContextPtr<> & context,
            const Tp::AccountPtr &account,
            const Tp::ConnectionPtr &connection,
            const QList<Tp::ChannelPtr> &channels,
            const QList<Tp::ChannelRequestPtr> &channelRequests,
            const QDateTime &userActionTime,
            const Tp::AbstractClientHandler::HandlerInfo &handlerInfo)
{
  qDebug() << "Channel REQUEST";
  Tp::TextChannelPtr textChannel;
  Q_FOREACH(const Tp::ChannelPtr & channel, channels) {
    textChannel = Tp::TextChannelPtr::dynamicCast(channel);
    if (textChannel) {
      break;
    }
  }

  Q_ASSERT(textChannel);

  if(mChats.contains(textChannel->targetId()) && mChats.value(textChannel->targetId()))
  {
    qDebug() << "chat window already exists, ignoring request" << textChannel->targetId();
  }
  else
  {
    qDebug() << "opening chat window" << textChannel->targetId();
    mChats.insert(textChannel->targetId(), new ChatWindow(textChannel, textChannel->targetContact(), account));
  }

  context->setFinished();
}

bool KMessApplication::bypassApproval() const
{
  return false;
}

int KMessApplication::exec(void)
{
  connect(mAccountManager->becomeReady(), SIGNAL(finished(Tp::PendingOperation *)),
      SLOT(onAccountManagerReady(Tp::PendingOperation *)));
  RosterWindow w(mAccount, this);
  w.show();

  qDebug() << "BEFORE EXEC";

  return QApplication::exec();
}

void KMessApplication::connectAccount(QString accountPath)
{
    qDebug() << "Connecting with" << accountPath;
    mAccount = Tp::Account::create(TP_QT_ACCOUNT_MANAGER_BUS_NAME, accountPath,
            mConnectionFactory, mChannelFactory, mContactFactory);

    connect((const QObject*) mAccount->becomeReady(mAccountFeatures),
        SIGNAL(finished(Tp::PendingOperation *)),
        SLOT(onAccountReady(Tp::PendingOperation *)));

    connect(mAccount.data(),
        SIGNAL(connectionChanged(Tp::ConnectionPtr)),
        SLOT(onConnectionChanged(Tp::ConnectionPtr)));
}

void KMessApplication::connectAccount(Tp::AccountPtr account)
{
}

void KMessApplication::onAccountReady(Tp::PendingOperation *op)
{
    if (op->isError()) {
        qWarning() << "Account cannot become ready - " <<
            op->errorName() << '-' << op->errorMessage();
        return;
    }


    qDebug() << "ON ACCOUNT READY";

    if (mAccount->connection().isNull()) {
        qDebug() << "The account given has no Connection. Setting it to online.";
        connect((const QObject*) mAccount->setRequestedPresence(Tp::Presence::available()),
            SIGNAL(finished(Tp::PendingOperation *)),
            SLOT(onAccountOnline(Tp::PendingOperation *)));
    }
    else
    {
      onAccountOnline(NULL);
    }
}

void KMessApplication::onAccountOnline(Tp::PendingOperation *op)
{
    if (op && op->isError()) {
        qWarning() << "Account cannot become ready - " <<
            op->errorName() << '-' << op->errorMessage();
        return;
    }
    qDebug() << "ON ACCOUNT ONLINE";
}

void KMessApplication::onConnectionChanged(Tp::ConnectionPtr conn)
{
    if(conn)
    {
      connect(mAccount->connection().data(),
          SIGNAL(statusChanged(Tp::ConnectionStatus)),
          SLOT(onConnectionStatusChanged(Tp::ConnectionStatus)));
    }
    onConnectionStatusChanged(mAccount->connectionStatus());
}

void KMessApplication::onConnectionStatusChanged(Tp::ConnectionStatus status)
{
  qDebug() << "Connection status changed to" << status;
  if(mAccount->connection())
  {
    emit connectionStatusChanged(mAccount, status);
  }
}
void KMessApplication::onAccountManagerReady(Tp::PendingOperation *)
{
    qDebug() << "AccountManagerReady";

    foreach (const Tp::AccountPtr &acc, mAccountManager->allAccounts())
    {
      qDebug()<< acc->displayName() << acc->objectPath();
      mAccounts->appendRow(new AccountItem(acc, mAccounts));
    }
}
