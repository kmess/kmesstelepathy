/***************************************************************************
                          account-item.h -  description
                             -------------------
    begin                : Monday 9 April 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _KMESS_ACCOUNT_ITEM_H_
#define _KMESS_ACCOUNT_ITEM_H_

#include <QStandardItem>

#include <TelepathyQt/Account>
#include <TelepathyQt/Types>

class QIcon;

class AccountItem : public QObject, public QStandardItem
{
  Q_OBJECT

public:
  AccountItem(const Tp::AccountPtr &account, QStandardItemModel *parent);
  ~AccountItem();
  QVariant data(int role) const;

public:
  enum Role
  {
    PathRole = Qt::UserRole,
    AvatarDataRole,
  };

private slots:
  /// Update Item when account is updated
  void onAccountUpdated();

private:
  /// Account represented by this item
  Tp::AccountPtr mAccount;
};

#endif
