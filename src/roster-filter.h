/***************************************************************************
                          roster-filter.h -  description
                             -------------------
    begin                : Sunday 19 February 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QSortFilterProxyModel>

class QModelIndex;

class RosterFilter : public QSortFilterProxyModel
{
    Q_OBJECT

public:
  RosterFilter(QObject *parent = 0);

protected:
  bool filterAcceptsRow( int sourceRow, const QModelIndex& sourceParent ) const;
  bool lessThan( const QModelIndex & left, const QModelIndex & right ) const;

public:
  void setShowOfflineContacts(bool show);
  bool isShowOfflineContacts(void) const;

private:
  bool mShowOfflineContacts;
};
