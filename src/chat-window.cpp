/***************************************************************************
                          chat-window.cpp -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "chat-window.h"
#include "richtextparser.h"

#include <QtGui/QKeyEvent>
#include <QtXmlPatterns/QXmlQuery>
#include <QtWebKit/QWebFrame>
#include <QDesktopServices>

ChatWindow::ChatWindow(const Tp::TextChannelPtr channel, const Tp::ContactPtr contact, const Tp::AccountPtr account, QWidget *parent)
  : QMainWindow(),
    mContact(contact),
    mAccount(account)
{
    mTextChannel = channel;
    setWindowTitle(contact->alias().append(" - KMess Telepathy - Chat"));
    setupGui();
}

ChatWindow::~ChatWindow()
{
  qDebug() << "DESTROY";
}

void ChatWindow::closeEvent ( QCloseEvent * event )
{
  qDebug() << "CLOSE";
  mTextChannel->requestLeave();
  deleteLater();
}


void ChatWindow::setupGui()
{
    mCentralWidget = new QWidget();
    ui = new Ui::mChatView();
    ui->setupUi(mCentralWidget);

    connect(ui->mMessageView, SIGNAL(loadFinished(bool)),
        SLOT(themeLoadFinished(bool)));

    setCentralWidget(mCentralWidget);

    ui->mMessageView->load(QUrl( "file://" + QCoreApplication::applicationDirPath() + "/../share/apps/kmess-telepathy/chatstyles/Fresh/Fresh.html"));
    connect(ui->mMessageView,
        SIGNAL(linkClicked(const QUrl &)),
        SLOT(linkClicked(const QUrl &)));

    installEventFilter(this);
    ui->mMessageEdit->installEventFilter(this);
    ui->mMessageEdit->setFocus();

    connect(ui->mSendButton, SIGNAL(clicked()), SLOT(sendMessage()));
    connect(ui->mNewLineButton, SIGNAL(clicked()), SLOT(insertNewLine()));

    mMessageViewFrame = ui->mMessageView->page()->mainFrame();
    ui->mMessageView->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);

    QWebSettings::globalSettings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);

    show();
}

void ChatWindow::linkClicked(const QUrl& url)
{
  qDebug() << "Link Clicked";
  QDesktopServices::openUrl(url);
}

void ChatWindow::sendMessage(void)
{
  QString text = ui->mMessageEdit->toPlainText();
  qDebug() << "Send message" << text;
  mTextChannel->send(text);
  QString avatar = QString();
  if(mTextChannel->groupSelfContact()->isAvatarTokenKnown())
  {
    avatar = mTextChannel->groupSelfContact()->avatarData().fileName;
  }
  showTextMessage(
      mAccount->nickname(),
      avatar,
      false, /*outgoing*/
      QDateTime::currentDateTime(),
      text
      );
  ui->mMessageEdit->setText(QString());
}

void ChatWindow::themeLoadFinished(bool)
{
  qDebug() << "Theme Loaded";
  QList<Tp::ReceivedMessage> queue = mTextChannel->messageQueue();
  foreach( const Tp::ReceivedMessage &message, queue )
  {
    messageReceived(message);
  }
  connect(mTextChannel.data(), SIGNAL(messageReceived(const Tp::ReceivedMessage &)),
      SLOT  (messageReceived(const Tp::ReceivedMessage &)), Qt::UniqueConnection);
}

bool ChatWindow::messageReceived(const Tp::ReceivedMessage &message)
{
  qDebug() << "message received" << message.text() << message.isDeliveryReport();
  if(!message.isDeliveryReport())
  {
    QString avatar = QString();
    if(message.sender()->isAvatarTokenKnown())
    {
      avatar = message.sender()->avatarData().fileName;
    }

    showTextMessage(
        message.senderNickname(),
        avatar,
        true, /*incoming*/
        message.received(),
        message.text()
        );
    QApplication::alert(this);
  }
  else
  {
    qDebug() << "ignored delivery report";
  }

  //Acknowledge message towards Telepathy
  QList<Tp::ReceivedMessage> list = QList<Tp::ReceivedMessage>();
  list.append(message);
  mTextChannel->acknowledge(list);
}

void ChatWindow::showTextMessage(QString nickname, QString avatar, bool isIncoming, QDateTime datetime, QString text)
{
    QVariant f1result = mMessageViewFrame->evaluateJavaScript(
        QString("addMessage(\'")
        .append(Qt::escape(nickname.replace('\'',"\\'")))
        .append("\', \'")
        .append(avatar)
        .append("\', \'")
        .append(datetime.toString("h:mm"))
        .append(isIncoming?"\',\'incoming\', \'":"\',\'outgoing\', \'")
        .append(RichTextParser::parse(Qt::escape(text.replace('\'',"\\'")), isIncoming?RichTextParser::IncomingChatMessage:RichTextParser::OutgoingChatMessage)
          .replace("\n", "<br />"))
        .append("\')"));
}

bool ChatWindow::eventFilter( QObject *obj, QEvent *event )
{
  ui->mMessageEdit->setFocus();
  switch( event->type() )
  {
    case QEvent::KeyPress:
      {
        QKeyEvent *keyEvent = static_cast<QKeyEvent*>( event );
        int key = keyEvent->key();

        if( (key == Qt::Key_Return || key == Qt::Key_Enter ) && ((keyEvent->modifiers() & Qt::ShiftModifier) == 0) )
        {
          sendMessage();
          return true;
        }
      }
      break;
    default:
      break;
  }
  return false;
}

void ChatWindow::insertNewLine(void)
{
  ui->mMessageEdit->insertPlainText( "\n" );
}
