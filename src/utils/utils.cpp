/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Antonio Nastasi <sifcenter@gmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file utils.cpp
 */

#include "utils.h"
//#include "debug/libkmessdebug.h"
//#include "utils/utils_internal.h"

//#include <KMess/NetworkGlobals>
//#include <KMess/Utils>

#include <QByteArray>
#include <QString>
#include <QUuid>
#include <QFile>
#include <QCryptographicHash>

#include <ctime>

using namespace KMess;

// Settings for debugging
#define NOTIFICATION_HIDE_P2P_SUPPORT 0

// Internal variables used by Utils
//static ClientCapabilities clientCapabilities_;
static QString            clientName_;
static QString            clientVersion_;
//static ChatLoggingMode    clientLogging_ = LoggingInactive;



/**
 * Extract a nonce from a QByteArray of data.
 * @todo documentation
 */
/*
QString Utils::extractNonce(const QByteArray &data, const int offset)
{
  KMESS_ASSERT( data.size() >= offset + 15 );

  const int noncePos[] = { 3,2,1,0            // Field 1 reversed
                         , 5,4                // Field 2 reversed
                         , 7,6                // Field 3 reversed
                         , 8,9                // field 4
                         , 10,11,12,13,14,15  // field 5
                         };

  const char hexMap[] = "0123456789ABCDEF";
  QString hex;
  for(int i = 0; i < 16; i++)
  {
    if( i == 4 || i == 6 || i == 8 || i == 10 )
    {
      hex += '-';
    }
    int upper = (data[offset + noncePos[i]] & 0xf0) >> 4;
    int lower = (data[offset + noncePos[i]] & 0x0f);
    hex += hexMap[upper];
    hex += hexMap[lower];
  }

  return '{' + hex + '}';
}



// Return the hash of a file name using the SHA1 algorithm
QByteArray Utils::generateFileHash( const QString &fileName )
{
  // Read the file's contents into a byte array
  QFile file( fileName );
  if( ! file.open( QIODevice::ReadOnly ) )
  {
    return QByteArray();
  }

  QByteArray fileData( file.readAll() );

  file.close();

  // Retrieve and return the file's hash
  return QCryptographicHash::hash( fileData, QCryptographicHash::Sha1 );
}
*/


/**
 * @brief Generate a random GUID.
 *
 * This is used in MSNP2P for the Call ID and Branch ID.
 *
 * @return A randomly generated GUID value.
 */
/*
QString KMess::Utils::generateGUID()
{
  return QUuid::createUuid().toString().toUpper();
}
*/


/**
 * @brief Generate an random number to use as ID.
 *
 * For use in MSNP2P, the value will not be below 4
 *
 * @return A random value between 4 and RAND_MAX.
 */

/*
quint32 KMess::Utils::generateID()
{
  // seed the RNG properly.
  qsrand( time(NULL) );
  return ( qrand() + 100 );
}
*/


/**
 * Return the client capabilities.
 *
 * @return ClientCapabilities
 */




/**
 * Return the client name.
 *
 * This string will be sent to other clients in chat metadata.
 *
 * @return QString
 */



/**
 * Return the client version number.
 *
 * This string will be sent to other clients in chat metadata.
 *
 * @return QString
 */



/**
 * Converts a string with HTML to one with escaped entities
 *
 * Only the main HTML control characters are escaped; the string is made suitable for
 * insertion within an HTML tag attribute.
 * Neither KDE nor Qt have escape/unescape methods this thorough, they only replace the <>&. Annoying.
 *
 * @param string  The string to modify
 * @return        A reference to the modified string
 */
QString &Utils::htmlEscape( QString &string )
{
  string.replace( ';', "&#59;" )
        .replace( '&', "&amp;" )
        .replace( "&amp;#59;", "&#59;" )
        .replace( '<', "&lt;"  )
        .replace( '>', "&gt;"  )
        .replace( '\'', "&#39;" )
        .replace( '"', "&#34;" );  // NOTE: by not using &quot; this result is also usable for XML.

  return string;
}



/**
 * Converts a string constant with HTML to one with escaped entities
 *
 * This version is suitable for use with string constants, as it creates a copy of the original string.
 *
 * @param string  The string to modify
 * @return        Another string with the escaped contents
 */
QString Utils::htmlEscape( const QString &string )
{
  QString copy( string );
  return htmlEscape( copy );
}



/**
 * Converts a string with escaped entities to one with HTML
 *
 * Only the main HTML control characters are unescaped; the string is reverted back to its
 * original state.
 * Neither KDE nor Qt have HTML to text decoding methods. Annoying.
 *
 * @param string  The string to modify
 * @return        A reference to the modified string
 */
QString &Utils::htmlUnescape( QString &string )
{
  string.replace( "&#34;", "\"" )
        .replace( "&#39;", "'"  )
        .replace( "&gt;",  ">"  )
        .replace( "&lt;",  "<"  )
        .replace( "&amp;", "&"  )
        .replace( "&#59;", ";"  );

  return string;
}



/**
 * Converts a string with escaped entities to one with HTML
 *
 * This version is suitable for use with string constants, as it creates a copy of the original string.
 *
 * @param string  The string to modify
 * @return        Another string with the escaped contents
 */
QString Utils::htmlUnescape( const QString &string )
{
  QString copy( string );
  return htmlUnescape( copy );
}

/**
 * Use this to inform the library of your client name and version This information is broadcasted to clients that
 * you chat to.
 *
 * If you do not set these details the library will provide its own defaults based on its
 * name and current version.
 *
 * Use setClientCapabilities and setClientLogging to specify your client's logging settings and
 * individual capabilities.
 *
 * @see setClientCapabilties
 * @see setClientLogging
 * @param name Name of your application
 * @param version Version of your application
 */
void Utils::setClientName( const QString &name, const QString &version )
{
  clientName_ = name;
  clientVersion_ = version;
}



