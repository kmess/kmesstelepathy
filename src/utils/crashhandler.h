/***************************************************************************
                          crashhandler.h -  description
                             -------------------
    begin                : Mon Apr 21 2008
    copyright            : (C) 2008 by Diederik van der Boor
    email                : "vdboor" --at-- "codingdomain.com"
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QLatin1String>



/**
 * @brief Custom handling of crashes.
 *
 * The kmessCrashed() function in this class is called when a crash occurs.
 * This allows us to produce custom reports with all information we need.
 *
 * @author Diederik van der Boor, based on Amarok code.
 * @ingroup Utils
 * @todo  The GUI dialogs still need to be implemented.
 */
class CrashHandler
{
  public:
    // Activate the handler
    static void        activate();
    // The crash handler C function
    static void        kmessCrashed( int signal );
    // Assign the app name
    static void        setAppName( const QLatin1String &appName );

  private:
    static QLatin1String appName_;
};


