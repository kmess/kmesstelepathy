/* *************************************************************************
 * This file is part of the KMess project.                                 *
 * (C) Copyright Antonio Nastasi <sifcenter@gmail.com>                     *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ************************************************************************* */

/**
 * @file utils.h
 */


#ifndef UTILS_H
#define UTILS_H

#include <QtGlobal>

/*
#include <KMess/ClientCapabilities>
#include <KMess/NetworkGlobals>
*/
class QString;


namespace KMess
{

  class Utils
  {
    public: // Static public method
      // Return the hash of a file name using the SHA1 algorithm
      static QByteArray      generateFileHash( const QString &fileName );
      // Generate a random GUID.
      static QString         generateGUID();
      // Generate an random number to use as ID
      static quint32         generateID();

      /*
      // Return the client capabilities
      static const ClientCapabilities getClientCapabilities();
      static ChatLoggingMode getClientLogging();
      */
      static const QString   getClientName();
      static const QString   getClientVersion();
      // Converts a string with HTML to one with escaped entities
      static QString        &htmlEscape( QString &string );
      // Converts a string constant with HTML to one with escaped entities
      static QString         htmlEscape( const QString &string );
      // Converts a string with escaped entities to one with HTML
      static QString        &htmlUnescape( QString &string );
      // Converts a string constant with escaped entities to one with HTML
      static QString         htmlUnescape( const QString &string );
      // Extract the nonce from a QByteArray
      static QString         extractNonce( const QByteArray &buffer, const int offset = 32 );
      // These three methods inform the library of the client name/version, logging mode
      // and capabilities. Passed to other clients.
      static void            setClientName( const QString &name, const QString &version );
      /*
      static void            setClientLogging( ChatLoggingMode loggingMode );
      static void            setClientCapabilities( ClientCapabilities::CapabilitiesFlags capabilities, ClientCapabilities::CapabilitiesExtendedFlags extendedCapabilities = ClientCapabilities::CXF_NONE );
      */

  };
}


#endif // UTILS_H
