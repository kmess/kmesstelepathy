/***************************************************************************
                          roster-filter.cpp -  description
                             -------------------
    begin                : Sunday 19 February 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "roster-filter.h"
#include "roster-item.h"

#include <QDebug>
#include <QStandardItemModel>

RosterFilter::RosterFilter(QObject *parent)
  : QSortFilterProxyModel(parent),
    mShowOfflineContacts(false)
{
}

bool RosterFilter::filterAcceptsRow( int sourceRow, const QModelIndex& sourceParent ) const
{
  QStandardItemModel *model = (QStandardItemModel*)sourceModel();

  //Always show groups
  if(model->itemFromIndex(sourceParent) == NULL)
  {
    return true;
  }
  uint offline = Tp::Presence::offline().barePresence().type;

  //Hide offline contacts
  if(!mShowOfflineContacts && sourceParent.child(sourceRow, 0).data(RosterItem::PresenceTypeRole).toUInt() == offline)
  {
    return false;
  }
  return QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent);
}

bool RosterFilter::lessThan( const QModelIndex & left, const QModelIndex & right ) const
{
  if(left.data(RosterItem::TypeRole) == RosterItem::ContactType && left.data(RosterItem::TypeRole) == RosterItem::ContactType)
  {
    //Prioritize online contacts
    //Offline contacts are not sorted
    uint offline = Tp::Presence::offline().barePresence().type;

    if(left.data(RosterItem::PresenceTypeRole).toUInt() == offline)
    {
      return false;
    }
    else if (right.data(RosterItem::PresenceTypeRole).toUInt() == offline)
    {
      return true;
    }
  }
  else
  {
    // Hack to put "Other Contacts" as last item.
    if(left.data() == "Other Contacts")
    {
      return false;
    }
    else if(left.data() == "Other Contacts")
    {
      return true;
    }
  }

  return QSortFilterProxyModel::lessThan(left, right);
}

void RosterFilter::setShowOfflineContacts(bool show)
{
  if(mShowOfflineContacts != show)
  {
    mShowOfflineContacts = show;
    invalidateFilter();
  }
}

bool RosterFilter::isShowOfflineContacts(void) const
{
  return mShowOfflineContacts;
}

