#ifndef _STATUS_H_
#define _STATUS_H_

#include <QHash>
#include <QMap>
#include <QObject>

class QAction;
class QIcon;
class QMenu;
class QString;

namespace Tp
{
  class Presence;
}

class Status : public QObject
{
  Q_OBJECT

  private:
    Status();
    Status& operator=(const Status &);

    static QMap<QString,QIcon> *mStatusMap;
    static QMenu *mStatusMenu;
    static Status *mInstance;
    static QHash<QString, QAction*> *mStatusMap2;
    static QHash<Tp::Presence, QIcon> *mStatusMap3;

    static Tp::Presence mPresenceDnd;
    static Tp::Presence mPresenceLunch;
    static Tp::Presence mPresencePhone;

  public:
    static QMap<QString,QIcon> *statusMap();
    static QMenu *statusMenu();
    static Status *instance();

  private slots:
    void statusClicked(QAction* action);

  signals:
    void statusRequestedByUser(QAction* action);
};
#endif
