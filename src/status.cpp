#include "status.h"

#include <QIcon>
#include <QMenu>

#include <TelepathyQt/Presence>

Tp::Presence Status::mPresenceDnd = Tp::Presence(Tp::ConnectionPresenceTypeExtendedAway,"dnd", "");
Tp::Presence Status::mPresenceLunch = Tp::Presence(Tp::ConnectionPresenceTypeExtendedAway, "lunch", "");
Tp::Presence Status::mPresencePhone = Tp::Presence(Tp::ConnectionPresenceTypeExtendedAway, "phone", "");

Status::Status()
    : QObject()
{
  return;
}

QMap<QString,QIcon> *Status::mStatusMap( 0 );
QHash<QString, QAction*> *Status::mStatusMap2( 0 );
QHash<Tp::Presence, QIcon> *Status::mStatusMap3( 0 );

QMap<QString,QIcon> *Status::statusMap()
{
  if(mStatusMap != 0)
  {
    return mStatusMap;
  }

  mStatusMap = new QMap<QString,QIcon>();
  mStatusMap3 = new QHash<Tp::Presence,QIcon>();

  mStatusMap->insert(Tp::Presence::available().status(), QIcon::fromTheme("user-online"));
  mStatusMap->insert(Tp::Presence::busy().status(), QIcon::fromTheme("user-busy"));
  mStatusMap->insert(Tp::Presence::away().status(), QIcon::fromTheme("user-away"));
  mStatusMap->insert(Tp::Presence::brb().status(), QIcon::fromTheme("user-away"));
  mStatusMap->insert(Tp::Presence::xa().status(), QIcon::fromTheme("user-away-extended"));
  mStatusMap->insert(Tp::Presence::hidden().status(), QIcon::fromTheme("user-invisible"));
  mStatusMap->insert("dnd", QIcon::fromTheme("user-busy"));
  mStatusMap->insert("lunch", QIcon::fromTheme("user-away-extended"));
  mStatusMap->insert("phone", QIcon::fromTheme("user-away-extended"));
  mStatusMap->insert(Tp::Presence::offline().status(), QIcon::fromTheme("user-offline"));

  //mStatusMap3->insert(Tp::Presence::available(), QIcon::fromTheme("user-online"));
  //mStatusMap3->insert(Tp::Presence::busy(), QIcon::fromTheme("user-busy"));
  //mStatusMap3->insert(Tp::Presence::away(), QIcon::fromTheme("user-away"));
  //mStatusMap3->insert(Tp::Presence::brb(), QIcon::fromTheme("user-away"));
  //mStatusMap3->insert(Tp::Presence::xa(), QIcon::fromTheme("user-away-extended"));
  //mStatusMap3->insert(Tp::Presence::hidden(), QIcon::fromTheme("user-invisible"));
  //mStatusMap3->insert(mPresenceDnd, QIcon::fromTheme("user-busy"));
  //mStatusMap3->insert(mPresenceLunch, QIcon::fromTheme("user-away-extended"));
  //mStatusMap3->insert(mPresencePhone, QIcon::fromTheme("user-away-extended"));
  //mStatusMap3->insert(Tp::Presence::offline(), QIcon::fromTheme("user-offline"));

  return mStatusMap;
}

QMenu* Status::mStatusMenu( 0 );

QMenu* Status::statusMenu()
{
  qDebug() << "status" << mStatusMenu;
  if(mStatusMenu != 0)
  {
    qDebug() << "status not null" << mStatusMenu;
    return mStatusMenu;
  }

  mStatusMenu = new QMenu("My Status");

  QIcon::setThemeName("oxygen");
  /*QMap<QString, QIcon>::iterator i;
  for (i = mStatusMap->begin(); i != mStatusMap->end(); ++i)
  {
    QAction* action = new QAction(i.value(), i.key(), mStatusMenu);
    mStatusMenu->addAction(action);

  }*/
  QAction* action;

  action = new QAction(QIcon::fromTheme("user-online"), "Online", mStatusMenu);
  action->setData(QVariant::fromValue(Tp::Presence::available()));
  mStatusMenu->addAction(action);

  action = new QAction(QIcon::fromTheme("user-away"), "Away", mStatusMenu);
  action->setData(QVariant::fromValue(Tp::Presence::away()));
  mStatusMenu->addAction(action);

  action = new QAction(QIcon::fromTheme("user-busy"), "Busy", mStatusMenu);
  action->setData(QVariant::fromValue(Tp::Presence::busy()));
  mStatusMenu->addAction(action);

  action = new QAction(QIcon::fromTheme("user-offline"), "Disconnect", mStatusMenu);
  action->setData(QVariant::fromValue(Tp::Presence::offline()));
  mStatusMenu->addAction(action);

  connect( mStatusMenu, SIGNAL(triggered(QAction *)),
           Status::instance(), SLOT(statusClicked(QAction *)));
  connect( mStatusMenu, SIGNAL(triggered(QAction *)),
           Status::instance(), SIGNAL(statusRequestedByUser(QAction *)));
  return mStatusMenu;
}

Status* Status::mInstance = 0;

Status* Status::instance()
{
  if(Status::mInstance != 0)
  {
    return Status::mInstance;
  }

  Status::mInstance = new Status();
  return Status::mInstance;
}

void Status::statusClicked(QAction* action)
{
  qDebug() << "PRESSED " << action->text();
}
