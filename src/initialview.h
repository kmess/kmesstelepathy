/***************************************************************************
                          initialview.h -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <QStandardItemModel>
#include <QWidget>

#include "src/ui_initialview.h"

#include <TelepathyQt/AccountManager>

class KMessApplication;

class InitialView: public QWidget
{
    Q_OBJECT

public:
    InitialView(KMessApplication* app);
    ~InitialView();

private:
    /// Setup UI
    void setupGui();

    Ui::mInitialView ui;
    KMessApplication* mApplication;
    /// Mouse event over the avatar
    void wheelEvent(QWheelEvent * event);
    /// List of accounts
    QStandardItemModel *mAccounts;

private slots:
    /// Handle request to connect to the selected account
    void onConnectRequest();
    /// Update interface when another account is selected
    void onAccountChanged(int highlighted);
    /// Account connection has changed
    void onConnectionStatusChanged(Tp::AccountPtr account, Tp::ConnectionStatus status);
    /// Account model has changed
    void onAccountModelChanged();
};
