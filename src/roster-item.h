/***************************************************************************
                          roster-item.h -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _KMESS_ROSTER_ITEM_H_
#define _KMESS_ROSTER_ITEM_H_

class QStandardItemModel;

#include <TelepathyQt/Account>
#include <TelepathyQt/Contact>
#include <TelepathyQt/ContactMessenger>
#include <TelepathyQt/Types>
#include <QList>
#include <QStandardItem>
#include <QString>

class RosterItem : public QObject, public QStandardItem
{
    Q_OBJECT

public:
    RosterItem(const Tp::ContactPtr &contact, QStandardItemModel *parent = 0);
    ~RosterItem();

    Tp::ContactPtr contact() const { return mContact; }
    QVariant data(int role) const;

signals:
    void changed();

public:
    enum Type
    {
      ContactType,
      GroupType
    };

    enum Role
    {
      TypeRole = Qt::UserRole,
// Contact Roles
      AvatarFileRole,
      AliasRole,
      BlockedRole,
      IdRole,
      PresenceTypeRole,
      PresenceStatusRole,
      PresenceStatusMessageRole,
// Group Roles
      ContactsRole,
      ContactsOnlineRole,
      ExpandedRole,
    };

private slots:
    void onContactChanged();

private:
    Tp::ContactPtr mContact;
    Tp::AccountPtr mAccount;
    Tp::ContactMessengerPtr mMessenger;

    QStandardItemModel *mModel;
    static QStandardItem *mOtherGroup;
    bool mOnline;
};

#endif
