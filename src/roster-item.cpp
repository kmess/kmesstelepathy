/***************************************************************************
                          roster-item.cpp -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "roster-item.h"

#include <QStandardItemModel>
#include <QTextStream>

#include <TelepathyQt/Presence>

using namespace Tp;

QStandardItem *RosterItem::mOtherGroup = NULL;

RosterItem::RosterItem(const ContactPtr &contact, QStandardItemModel *parent)
    : QObject(parent),
      QStandardItem(/*parent*/),
      mContact(contact),
      mModel(parent),
      mOnline(false)
{
    if(mOtherGroup == NULL)
    {
        QStandardItem* groupitem = new QStandardItem(QString("Other Contacts"));
        groupitem->setData(QVariant(QString("Other Contacts")), Qt::DisplayRole);
        groupitem->setData(GroupType, TypeRole);
        groupitem->setData(1, ContactsRole);

        mOtherGroup = groupitem;
        mModel->invisibleRootItem()->insertRow(0, groupitem);
        groupitem->setData(true, ExpandedRole);
    }

    setData(QVariant(contact->id()));
    onContactChanged();

    connect(contact.data(),
            SIGNAL(aliasChanged(QString)),
            SLOT(onContactChanged()));
    connect(contact.data(),
            SIGNAL(subscriptionStateChanged(Tp::Contact::PresenceState)),
            SLOT(onContactChanged()));
    connect(contact.data(),
            SIGNAL(publishStateChanged(Tp::Contact::PresenceState,QString)),
            SLOT(onContactChanged()));
    connect(contact.data(),
            SIGNAL(blockStatusChanged(bool)),
            SLOT(onContactChanged()));
    connect(contact.data(),
            SIGNAL(avatarDataChanged(const Tp::AvatarData)),
            SLOT(onContactChanged()));
    connect(contact.data(),
            SIGNAL(presenceChanged(const Tp::Presence &)),
            SLOT(onContactChanged()));

    contact->requestAvatarData();
}

RosterItem::~RosterItem()
{
}

void RosterItem::onContactChanged()
{
    QString caption;
    QString status = mContact->presence().status();
    QStringList groups = mContact->groups();

    // I've asked to see the contact presence
    if (mContact->subscriptionState() == Contact::PresenceStateAsk) {
        caption = QString(QLatin1String("%1 (%2) (awaiting approval)")).arg(mContact->alias()).arg(status);
    // The contact asked to see my presence
    } else if (mContact->publishState() == Contact::PresenceStateAsk) {
        caption = QString(QLatin1String("%1 (%2) (pending approval)")).arg(mContact->alias()).arg(status);
    } else if (mContact->subscriptionState() == Contact::PresenceStateNo &&
               mContact->publishState() == Contact::PresenceStateNo) {
        caption = QString(QLatin1String("%1 (unknown)")).arg(mContact->alias());
    } else {
        caption = QString(QLatin1String("%1")).arg(mContact->alias());
    }

    if (mContact->isBlocked()) {
        caption = QString(QLatin1String("%1 (blocked)")).arg(caption);
    }

    setText(caption);

    bool online = (mContact->presence().type() != Tp::ConnectionPresenceTypeOffline);
    int change = 0;
    if(online != mOnline) // Online status changed
    {
      change = online?1:-1;
      mOnline = online;
    }

    // Hackish way of adding groups, but should do the trick for now
    if(groups.count())
    {
      foreach(QString group, groups)
      {
        int groupfound = FALSE;
        for(int i = 0 ; i < mModel->invisibleRootItem()->rowCount() ;i++)
        {
          QStandardItem* item = mModel->invisibleRootItem()->child(i, 0);
          QString grouptext = item->text();


          if(grouptext == group)
          {
            int contactfound = FALSE;
            for(int i = 0 ; i < item->rowCount();i++)
            {
              QStandardItem* contactitem = item->child(i, 0);
              if(contactitem == this)
              {
                contactfound = TRUE;
                groupfound = TRUE;
                if(change != 0)
                {
                  item->setData(item->data(ContactsOnlineRole).toInt()+change, ContactsOnlineRole);
                }
              }
            }
            if(contactfound == FALSE)
            {
              item->appendRow(this);
              groupfound = TRUE;
              item->setData(item->data(ContactsRole).toInt()+1, ContactsRole);
              if(change != 0)
              {
                item->setData(item->data(ContactsOnlineRole).toInt()+change, ContactsOnlineRole);
              }
            }
          }
        }

        if(groupfound == FALSE)
        {
          QStandardItem* groupitem = new QStandardItem(group);
          groupitem->setData(QVariant(group), Qt::DisplayRole);
          groupitem->setData(GroupType, TypeRole);

          mModel->invisibleRootItem()->appendRow(groupitem);
          QStandardItem* item = mModel->invisibleRootItem()->child((mModel->invisibleRootItem()->rowCount())-1);
          groupitem->setData(1, ContactsRole);
          if(change != 0)
          {
            groupitem->setData(item->data(ContactsOnlineRole).toInt()+change, ContactsOnlineRole);
          }

          item->appendRow(this);
          groupitem->setData(true, ExpandedRole);
        }
      }
    }
    else
    {
      // Add to "Other Contacts"
      mOtherGroup->appendRow(this);
      mOtherGroup->setData(mOtherGroup->data(ContactsRole).toInt()+1, ContactsRole);
      if(change != 0)
      {
        mOtherGroup->setData(mOtherGroup->data(ContactsOnlineRole).toInt()+change, ContactsOnlineRole);
      }

    }

    emitDataChanged();
}

QVariant RosterItem::data(int role) const
{
  if(role < Qt::UserRole)
  {
    return QStandardItem::data(role);
  }

  switch(role)
  {
    case TypeRole:
      return ContactType;
    case AliasRole:
      qDebug() << "Alias";
      break;
    case AvatarFileRole:
      return mContact->avatarData().fileName;
    case BlockedRole:
      return mContact->isBlocked();
    case IdRole:
      return mContact->id();
    case PresenceTypeRole:
      return mContact->presence().barePresence().type;
    case PresenceStatusRole:
      return mContact->presence().status();
    case PresenceStatusMessageRole:
      return mContact->presence().statusMessage();
    default:
      qWarning() << "Unsupported data request" << role;
      break;
  }
}
