/***************************************************************************
                          initialview.cpp -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "account-item.h"
#include "initialview.h"
#include "kmess-application.h"
#include "status.h"

#include <QDebug>
#include <QWheelEvent>

#include <TelepathyQt/PendingReady>

InitialView::InitialView(KMessApplication* app)
  :QWidget(),
  mAccounts(app->accounts()),
  mApplication(app)
{
    setupGui();

    ui.mAccounts->setModel(mAccounts);

    connect(mAccounts,
            SIGNAL(rowsInserted(const QModelIndex&, int, int)),
            SLOT(onAccountModelChanged()));
    connect(mAccounts,
            SIGNAL(rowsRemoved(const QModelIndex&, int, int)),
            SLOT(onAccountModelChanged()));

    connect(mApplication,
            SIGNAL(connectionStatusChanged(Tp::AccountPtr, Tp::ConnectionStatus)),
            SLOT(onConnectionStatusChanged(Tp::AccountPtr, Tp::ConnectionStatus)));
}

InitialView::~InitialView()
{
}

void InitialView::setupGui()
{
    ui = Ui::mInitialView();
    ui.setupUi(this);

    connect(ui.mConnect, SIGNAL(clicked(bool)),
            SLOT(onConnectRequest()));

    connect(ui.mAccounts, SIGNAL(currentIndexChanged(int)), SLOT(onAccountChanged(int)));
    onAccountModelChanged();
}

void InitialView::onAccountModelChanged()
{
  ui.mConnect->setEnabled(mAccounts->rowCount() > 0);
}

void InitialView::onConnectRequest()
{
    QString accountPath = ui.mAccounts->itemData(ui.mAccounts->currentIndex(), AccountItem::PathRole).value<QString>();
    qDebug() << "Request to connect with" << ui.mAccounts->currentText() << accountPath ;
    mApplication->connectAccount(accountPath);
}

void InitialView::onAccountChanged(int highlighted)
{
    QString accountPath = ui.mAccounts->itemData(highlighted).value<QString>();
    qDebug() << "initialview selected" << highlighted <<  accountPath;
    QPixmap pixmap = QPixmap();
    if(!pixmap.loadFromData(ui.mAccounts->itemData(highlighted, AccountItem::AvatarDataRole).value<QByteArray>()))
    {
      QString filepath = QCoreApplication::applicationDirPath() + "/../share/apps/kmess-telepathy/pics/unknown.png";
      pixmap = QPixmap(filepath);
      qDebug() << "No avatar found, showing default" << filepath;
    }
    ui.mAvatar->setPixmap(pixmap);
}

void InitialView::onConnectionStatusChanged(Tp::AccountPtr account, Tp::ConnectionStatus status)
{
  qDebug() << "connectionStatusChanged in initialview";

  switch (status)
  {
    case Tp::ConnectionStatusConnecting:
      qDebug() << "Connecting";
      ui.mConnect->setEnabled(false);
      ui.mMessage->setText("Connecting");
      break;
    case Tp::ConnectionStatusConnected:
      qDebug() << "Connected";
      break;
    case Tp::ConnectionStatusDisconnected:
      qDebug() << "initialview disconnected";
      /*switch(account->connectionStatusReason())
      {
        case ConnectionStatusReasonNoneSpecified:
        case ConnectionStatusReasonRequested:
          // Do nothing
          break;
        default:
          // Handle all errors at one for now
      }*/
      ui.mMessage->setText(account->connectionError());
      ui.mConnect->setEnabled(true);
      break;
  }
}

void InitialView::wheelEvent(QWheelEvent *event)
{
  int newIndex = ui.mAccounts->currentIndex()+(event->delta() > 0?-1:1);
  newIndex = (newIndex+ui.mAccounts->count())%ui.mAccounts->count();

  ui.mAccounts->setCurrentIndex( newIndex );
}
