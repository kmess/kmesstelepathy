/***************************************************************************
                          roster-window.cpp -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "kmess-application.h"
#include "chat-window.h"
#include "initialview.h"
#include "roster-item.h"
#include "roster-window.h"
#include "roster-widget.h"
#include "status.h"

#include <TelepathyQt/Types>
#include <TelepathyQt/ChannelFactory>
#include <TelepathyQt/Connection>
#include <TelepathyQt/ConnectionFactory>
#include <TelepathyQt/ContactFactory>
#include <TelepathyQt/PendingOperation>
#include <TelepathyQt/PendingReady>
#include <TelepathyQt/Presence>
#include <TelepathyQt/SimpleTextObserver>


#include <QDebug>
#include <QStandardItemModel>

using namespace Tp;

RosterWindow::RosterWindow(Tp::AccountPtr account, KMessApplication *application, QWidget *parent)
    : QMainWindow(parent),
      mAccount(account),
      mApplication(application)
{
    setWindowTitle(QLatin1String("KMess Telepathy 0.0.3"));

    setupGui();

    connect(mApplication,
            SIGNAL(connectionStatusChanged(Tp::AccountPtr, Tp::ConnectionStatus)),
            SLOT(onConnectionStatusChanged(Tp::AccountPtr, Tp::ConnectionStatus)));

    connect( Status::instance(), SIGNAL(statusRequestedByUser(QAction *)),
             this, SLOT(onStatusChange(QAction *)));

    resize(240, 320);
}

RosterWindow::~RosterWindow()
{
}

void RosterWindow::setupGui()
{
    setCentralWidget(new InitialView(mApplication));
}

void RosterWindow::onConnectionStatusChanged(Tp::AccountPtr account, Tp::ConnectionStatus status)
{
  qDebug() << "connectionStatusChanged in rosterwindow";
  switch(status)
  {
    case Tp::ConnectionStatusConnected:
      qDebug() << "Account ready";
      mAccount = account;
      mRoster = new RosterWidget(mAccount);
      setCentralWidget(mRoster);
      //onAccountConnectionChanged(mAccount->connection());
      mRoster->setConnection(account);
      setWindowTitle(mAccount->nickname() + " - KMess Telepathy");
      break;
    case Tp::ConnectionStatusDisconnected:
      setWindowTitle(QLatin1String("KMess Telepathy 0.0.3"));
      setupGui();
      break;
  }
}

void RosterWindow::onStatusChange(QAction *action)
{
  qDebug() << "Status change requested" << action->data().value<Tp::Presence>().status();
  mAccount->setRequestedPresence(action->data().value<Tp::Presence>());
}
