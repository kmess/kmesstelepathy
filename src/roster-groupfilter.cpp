/***************************************************************************
                          roster-groupfilter.cpp -  description
                             -------------------
    begin                : Monday 20 February 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "roster-groupfilter.h"

#include <QDebug>
#include <QStandardItemModel>

RosterGroupFilter::RosterGroupFilter( QObject * parent )
  :showEmptyGroups(true)
{
}

bool RosterGroupFilter::setShowEmptyGroups(bool show)
{
  showEmptyGroups = show;
  invalidateFilter();
}

bool RosterGroupFilter::filterAcceptsRow( int sourceRow, const QModelIndex& sourceParent ) const
{
  QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);

  if(!showEmptyGroups && !sourceParent.isValid())
  {
    //Show group when it has children
    return sourceModel()->hasChildren(index);
  }

  return true;
}

