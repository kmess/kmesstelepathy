/***************************************************************************
                          richtextparser.cpp -  description
                             -------------------
    begin                : Wednesday 1 February 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "richtextparser.h"

#include "msgparsers/emoticon.h"
#include "msgparsers/bbcode.h"
#include "msgparsers/url.h"

#include <QString>
#include <QStringList>

QString RichTextParser::parse(QString string, Context context)
{
  QStringList list = QStringList(string);
  list = Url::parse(list, true);
  list = BbCode::parse(list, true);
  list = Emoticon::parse(list, true);
  return list.join("");
}
