/***************************************************************************
                          roster-groupfilter.h -  description
                             -------------------
    begin                : Monday 20 February 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QSortFilterProxyModel>

class QModelIndex;

class RosterGroupFilter : public QSortFilterProxyModel
{
  Q_OBJECT

public:
  RosterGroupFilter( QObject * parent = 0 );
  bool setShowEmptyGroups(bool hide);

protected:
  bool filterAcceptsRow( int sourceRow, const QModelIndex& sourceParent ) const;

private:
  bool showEmptyGroups;
};

