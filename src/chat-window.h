/***************************************************************************
                          chat-window.h -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _CHAT_WINDOW_H_
#define _CHAT_WINDOW_H_

#include "src/ui_chatview.h"

#include <QMainWindow>

#include <TelepathyQt/Account>
#include <TelepathyQt/Contact>
#include <TelepathyQt/ContactMessenger>
#include <TelepathyQt/TextChannel>

class QKeyEvent;
class QWebFrame;

class ChatWindow : public QMainWindow
{
    Q_OBJECT

public:
    ChatWindow(const Tp::TextChannelPtr channel, const Tp::ContactPtr contact, const Tp::AccountPtr account, QWidget *parent = 0);
    virtual ~ChatWindow();

private:
    /// Setup UI
    void setupGui();

    const Tp::ContactPtr mContact;
    const Tp::AccountPtr mAccount;
    Tp::TextChannelPtr mTextChannel;

    QWidget *mCentralWidget;
    QWebFrame *mMessageViewFrame;
    Ui::mChatView* ui;
    /// Used to catch the "enter" to send message quickly
    void TextEditkeyPressEvent(QKeyEvent *event);
    /// Window is about to be closed, close chat properly too.
    void closeEvent ( QCloseEvent * event );
    /// Show text message in window
    void showTextMessage(QString nickname, QString avatar, bool isIncoming, QDateTime datetime, QString text);

private slots:
    /// Send entered text as message to our contact
    void sendMessage(void);
    /// Handle input events from user
    bool eventFilter( QObject *obj, QEvent *event );
    /// Received a message from our contact
    bool messageReceived(const Tp::ReceivedMessage &message);
    /// Chat theme finished loading
    void themeLoadFinished(bool);
    /// Linked clicked inside chat
    void linkClicked(const QUrl& url);
    /// Insert newline into the textbox
    void insertNewLine(void);
};


#endif
