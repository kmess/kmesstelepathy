/***************************************************************************
                          sasl-auth.h -  description
                             -------------------
    begin                : Saturday 7 April 2012
    copyright            : (C) 2011 by Andre Moreira Magalhaes
    copyright            : (C) 2011 by David Edmundson
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SASLAUTHOP_H
#define SASLAUTHOP_H

#include <TelepathyQt/Account>
#include <TelepathyQt/Channel>
#include <TelepathyQt/Connection>
#include <TelepathyQt/PendingOperation>
#include <TelepathyQt/Types>

class SaslAuthOp : public Tp::PendingOperation
{
    Q_OBJECT

public:
    SaslAuthOp(const Tp::AccountPtr &account,
            const Tp::ConnectionPtr &connection,
            const Tp::ChannelPtr &channel);
    ~SaslAuthOp();

signals:
    void ready(Tp::PendingOperation *self);

private slots:
    void gotProperties(Tp::PendingOperation *op);
    void onSASLStatusChanged(uint status, const QString &reason, const QVariantMap &details);

private:
    void promptUser();

    Tp::AccountPtr m_account;
    Tp::ConnectionPtr m_connection;
    Tp::ChannelPtr m_channel;
    Tp::Client::ChannelInterfaceSASLAuthenticationInterface *m_saslIface;
    bool m_canTryAgain;
};

#endif
