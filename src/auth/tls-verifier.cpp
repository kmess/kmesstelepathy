/***************************************************************************
                          tls-verifier.cpp -  description
                             -------------------
    begin                : Saturday 7 April 2012
    copyright            : (C) 2011 by Andre Moreira Magalhaes
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "tls-verifier.h"

#include <TelepathyQt/PendingVariantMap>

#include <QDebug>

#include <QByteArray>
#include <QList>
#include <QMetaType>

TlsCertVerifierOp::TlsCertVerifierOp(const Tp::AccountPtr &account,
        const Tp::ConnectionPtr &connection,
        const Tp::ChannelPtr &channel)
    : Tp::PendingOperation(channel),
      m_account(account),
      m_connection(connection),
      m_channel(channel)
{
    QString certificatePath = qdbus_cast<QString>(channel->immutableProperties().value(
                TP_QT_IFACE_CHANNEL_TYPE_SERVER_TLS_CONNECTION + ".ServerCertificate"));
    m_hostname = qdbus_cast<QString>(channel->immutableProperties().value(
                TP_QT_IFACE_CHANNEL_TYPE_SERVER_TLS_CONNECTION + ".Hostname"));
    m_referenceIdentities = qdbus_cast<QStringList>(channel->immutableProperties().value(
                TP_QT_IFACE_CHANNEL_TYPE_SERVER_TLS_CONNECTION + ".ReferenceIdentities"));

    m_authTLSCertificateIface = new Tp::Client::AuthenticationTLSCertificateInterface(
            channel->dbusConnection(), channel->busName(), certificatePath);
    connect(m_authTLSCertificateIface->requestAllProperties(),
            SIGNAL(finished(Tp::PendingOperation*)),
            SLOT(gotProperties(Tp::PendingOperation*)));

    static bool registered = false;
    if (registered)
      return;
    registered = true;

    qDBusRegisterMetaType<CertificateDataList>();
}

void TlsCertVerifierOp::gotProperties(Tp::PendingOperation *op)
{
    if (op->isError()) {
        qWarning() << "Unable to retrieve properties from AuthenticationTLSCertificate object at" <<
            m_authTLSCertificateIface->path();
        m_channel->requestClose();
        setFinishedWithError(op->errorName(), op->errorMessage());
        return;
    }

    qDebug() << "verifier got props";

    // everything ok, we can return from handleChannels now
    emit ready(this);

    Tp::PendingVariantMap *pvm = qobject_cast<Tp::PendingVariantMap*>(op);
    QVariantMap props = qdbus_cast<QVariantMap>(pvm->result());
    m_certType = qdbus_cast<QString>(props.value("CertificateType"));
    m_certData = qdbus_cast<CertificateDataList>(props.value("certificateChainData"));
    qDebug() << "CERT TYPE" << m_certType;
    qDebug() << "CERT DATA" << m_certData;

    // FIXME: verify cert
    setFinished();
}
