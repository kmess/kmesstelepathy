/***************************************************************************
                          tls-verifier.h -  description
                             -------------------
    begin                : Saturday 7 April 2012
    copyright            : (C) 2011 by Andre Moreira Magalhaes
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TLSCERTVERIFIEROP_H
#define TLSCERTVERIFIEROP_H

#include <TelepathyQt/Account>
#include <TelepathyQt/AuthenticationTLSCertificateInterface>
#include <TelepathyQt/Channel>
#include <TelepathyQt/Connection>
#include <TelepathyQt/PendingOperation>
#include <TelepathyQt/Types>

typedef QList<QByteArray> CertificateDataList;
Q_DECLARE_METATYPE(CertificateDataList)

class TlsCertVerifierOp : public Tp::PendingOperation
{
    Q_OBJECT

public:
    TlsCertVerifierOp(const Tp::AccountPtr &account,
            const Tp::ConnectionPtr &connection,
            const Tp::ChannelPtr &channel);

signals:
    void ready(Tp::PendingOperation *self);

private slots:
    void gotProperties(Tp::PendingOperation *op);

private:
    Tp::AccountPtr m_account;
    Tp::ConnectionPtr m_connection;
    Tp::ChannelPtr m_channel;
    QString m_hostname;
    QStringList m_referenceIdentities;
    Tp::Client::AuthenticationTLSCertificateInterface *m_authTLSCertificateIface;
    QString m_certType;
    CertificateDataList m_certData;
};

#endif
