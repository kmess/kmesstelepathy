/***************************************************************************
                          sasl-auth.cpp -  description
                             -------------------
    begin                : Saturday 7 April 2012
    copyright            : (C) 2011 by Andre Moreira Magalhaes
    copyright            : (C) 2011 by David Edmundson
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "sasl-auth.h"

#include <TelepathyQt/PendingVariantMap>

#include <QDebug>
#include <QInputDialog>

SaslAuthOp::SaslAuthOp(const Tp::AccountPtr &account,
        const Tp::ConnectionPtr &connection,
        const Tp::ChannelPtr &channel)
    : Tp::PendingOperation(channel),
      m_account(account),
      m_connection(connection),
      m_channel(channel),
      m_saslIface(channel->interface<Tp::Client::ChannelInterfaceSASLAuthenticationInterface>()),
      m_canTryAgain(false)
{
    connect(m_saslIface->requestAllProperties(),
            SIGNAL(finished(Tp::PendingOperation*)),
            SLOT(gotProperties(Tp::PendingOperation*)));
}

SaslAuthOp::~SaslAuthOp()
{
}

void SaslAuthOp::gotProperties(Tp::PendingOperation *op)
{
    if (op->isError()) {
        qWarning() << "Unable to retrieve available SASL mechanisms";
        m_channel->requestClose();
        setFinishedWithError(op->errorName(), op->errorMessage());
        return;
    }

    Tp::PendingVariantMap *pvm = qobject_cast<Tp::PendingVariantMap*>(op);
    QVariantMap props = qdbus_cast<QVariantMap>(pvm->result());
    m_canTryAgain = qdbus_cast<bool>(props.value("CanTryAgain"));
    QStringList mechanisms = qdbus_cast<QStringList>(props.value("AvailableMechanisms"));
    if (!mechanisms.contains(QLatin1String("X-TELEPATHY-PASSWORD"))) {
        qWarning() << "X-TELEPATHY-PASSWORD is the only supported SASL mechanism and "
            "is not available";
        m_channel->requestClose();
        setFinishedWithError(TP_QT_ERROR_NOT_IMPLEMENTED,
                QLatin1String("X-TELEPATHY-PASSWORD is the only supported SASL mechanism and "
                    " is not available"));
        return;
    }

    // everything ok, we can return from handleChannels now
    emit ready(this);

    connect(m_saslIface,
            SIGNAL(SASLStatusChanged(uint,QString,QVariantMap)),
            SLOT(onSASLStatusChanged(uint,QString,QVariantMap)));
    uint status = qdbus_cast<uint>(props.value("SASLStatus"));
    QString error = qdbus_cast<QString>(props.value("SASLError"));
    QVariantMap errorDetails = qdbus_cast<QVariantMap>(props.value("SASLErrorDetails"));
    onSASLStatusChanged(status, error, errorDetails);
}

void SaslAuthOp::onSASLStatusChanged(uint status, const QString &reason,
        const QVariantMap &details)
{
    if (status == Tp::SASLStatusNotStarted) {
        qDebug() << "Requesting password";
        promptUser();
    } else if (status == Tp::SASLStatusServerSucceeded) {
        qDebug() << "Authentication handshake";
        m_saslIface->AcceptSASL();
    } else if (status == Tp::SASLStatusSucceeded) {
        qDebug() << "Authentication succeeded";
        m_channel->requestClose();
        setFinished();
    } else if (status == Tp::SASLStatusInProgress) {
        qDebug() << "Authenticating...";
    } else if (status == Tp::SASLStatusServerFailed) {
        qDebug() << "Error authenticating - reason:" << reason << "- details:" << details;

        if (m_canTryAgain) {
            qDebug() << "Retrying...";
            promptUser();
        } else {
            qWarning() << "Authentication failed and cannot try again";
            //wallet.setEntry(m_account, QLatin1String("lastLoginFailed"), QLatin1String("true"));
            // We cannot try again, but we can request again to set the account
            // online. A new channel will be created, but since we set the
            // lastLoginFailed entry, next time we will prompt for password
            // and the user won't see any difference except for an
            // authentication error notification
            Tp::Presence requestedPresence = m_account->requestedPresence();
            m_channel->requestClose();
            QString errorMessage = details[QLatin1String("server-message")].toString();
            m_account->setRequestedPresence(requestedPresence);

            setFinishedWithError(reason, errorMessage.isEmpty() ? "Authentication error" : errorMessage);
        }
    }
}

void SaslAuthOp::promptUser()
{
    QInputDialog* dialog = new QInputDialog();
    dialog->setTextEchoMode(QLineEdit::Password);
    dialog->setLabelText("Password requested");
    if (dialog->exec() == QDialog::Rejected) {
      qDebug() << "Authentication cancelled";
      m_saslIface->AbortSASL(Tp::SASLAbortReasonUserAbort, "User cancelled auth");
      m_channel->requestClose();
      setFinished();
      return;
    }

    m_saslIface->StartMechanismWithData(QLatin1String("X-TELEPATHY-PASSWORD"), dialog->textValue().toUtf8());
}
