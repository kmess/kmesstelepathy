/***************************************************************************
                          tls-handler.cpp -  description
                             -------------------
    begin                : Saturday 7 April 2012
    copyright            : (C) 2011 by Andre Moreira Magalhaes
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "tls-handler.h"
#include "tls-verifier.h"

#include <QDBusConnection>

#include <TelepathyQt/Channel>
#include <TelepathyQt/ChannelDispatchOperation>
#include <TelepathyQt/MethodInvocationContext>

TlsHandler::TlsHandler(const Tp::ChannelClassSpecList &channelFilter)
    : Tp::AbstractClientHandler(channelFilter)
{
}

bool TlsHandler::bypassApproval() const
{
    return true;
}

void TlsHandler::handleChannels(const Tp::MethodInvocationContextPtr<> &context,
        const Tp::AccountPtr &account,
        const Tp::ConnectionPtr &connection,
        const QList<Tp::ChannelPtr> &channels,
        const QList<Tp::ChannelRequestPtr> &requestsSatisfied,
        const QDateTime &userActionTime,
        const Tp::AbstractClientHandler::HandlerInfo &handlerInfo)
{
    Q_UNUSED(requestsSatisfied);
    Q_UNUSED(userActionTime);
    Q_UNUSED(handlerInfo);

    Q_ASSERT(channels.size() == 1);
    qDebug() << "Handle TLS Channel";

    TlsCertVerifierOp *verifier = new TlsCertVerifierOp(
            account, connection, channels.first());
    connect(verifier,
            SIGNAL(ready(Tp::PendingOperation*)),
            SLOT(onCertVerifierReady(Tp::PendingOperation*)));
    connect(verifier,
            SIGNAL(finished(Tp::PendingOperation*)),
            SLOT(onCertVerifierFinished(Tp::PendingOperation*)));
    mVerifiers.insert(verifier, context);
}

void TlsHandler::onCertVerifierReady(Tp::PendingOperation *op)
{
    TlsCertVerifierOp *verifier = qobject_cast<TlsCertVerifierOp*>(op);
    Q_ASSERT(mVerifiers.contains(verifier));

    Tp::MethodInvocationContextPtr<> context = mVerifiers.value(verifier);
    context->setFinished();
    qDebug() << "Verifier ready";
}

void TlsHandler::onCertVerifierFinished(Tp::PendingOperation *op)
{
    TlsCertVerifierOp *verifier = qobject_cast<TlsCertVerifierOp*>(op);
    Q_ASSERT(mVerifiers.contains(verifier));

    if (op->isError()) {
        qWarning() << "Error verifying TLS certificate:" << op->errorName() << "-" << op->errorMessage();
    }

    mVerifiers.remove(verifier);
    qDebug() << "Verifier finished";

    qWarning() << "Certificate authenticity is not verified!";
}
