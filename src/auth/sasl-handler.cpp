/***************************************************************************
                          tls-handler.cpp -  description
                             -------------------
    begin                : Saturday 7 April 2012
    copyright            : (C) 2011 by Andre Moreira Magalhaes
    copyright            : (C) 2011 by David Edmundson
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "sasl-handler.h"

#include "sasl-auth.h"

#include <QDBusConnection>

#include <TelepathyQt/Channel>
#include <TelepathyQt/ChannelDispatchOperation>
#include <TelepathyQt/MethodInvocationContext>

#include <QDebug>

SaslHandler::SaslHandler(const Tp::ChannelClassSpecList &channelFilter)
    : Tp::AbstractClientHandler(channelFilter)
{
}

bool SaslHandler::bypassApproval() const
{
    return true;
}

void SaslHandler::handleChannels(const Tp::MethodInvocationContextPtr<> &context,
        const Tp::AccountPtr &account,
        const Tp::ConnectionPtr &connection,
        const QList<Tp::ChannelPtr> &channels,
        const QList<Tp::ChannelRequestPtr> &requestsSatisfied,
        const QDateTime &userActionTime,
        const Tp::AbstractClientHandler::HandlerInfo &handlerInfo)
{
    Q_UNUSED(requestsSatisfied);
    Q_UNUSED(userActionTime);
    Q_UNUSED(handlerInfo);

    Q_ASSERT(channels.size() == 1);

    SaslAuthOp *auth = new SaslAuthOp(
            account, connection, channels.first());
    connect(auth,
            SIGNAL(ready(Tp::PendingOperation*)),
            SLOT(onAuthReady(Tp::PendingOperation*)));
    connect(auth,
            SIGNAL(finished(Tp::PendingOperation*)),
            SLOT(onAuthFinished(Tp::PendingOperation*)));
    mAuthContexts.insert(auth, context);
}

void SaslHandler::onAuthReady(Tp::PendingOperation *op)
{
    SaslAuthOp *auth = qobject_cast<SaslAuthOp*>(op);
    Q_ASSERT(mAuthContexts.contains(auth));

    Tp::MethodInvocationContextPtr<> context = mAuthContexts.value(auth);
    context->setFinished();
}

void SaslHandler::onAuthFinished(Tp::PendingOperation *op)
{
    SaslAuthOp *auth = qobject_cast<SaslAuthOp*>(op);
    Q_ASSERT(mAuthContexts.contains(auth));

    if (op->isError()) {
        qWarning() << "Error in SASL auth:" << op->errorName() << "-" << op->errorMessage();
    }

    mAuthContexts.remove(auth);
}
