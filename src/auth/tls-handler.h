/***************************************************************************
                          tls-handler.h -  description
                             -------------------
    begin                : Saturday 7 April 2012
    copyright            : (C) 2011 by Andre Moreira Magalhaes
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef TLSHANDLER_H
#define TLSHANDLER_H

#include <QObject>

#include <TelepathyQt/AbstractClientHandler>

namespace Tp
{
    class PendingOperation;
};

class TlsHandler : public QObject, public Tp::AbstractClientHandler
{
    Q_OBJECT

public:
    explicit TlsHandler(const Tp::ChannelClassSpecList &channelFilter);

    bool bypassApproval() const;

    void handleChannels(const Tp::MethodInvocationContextPtr<> &context,
            const Tp::AccountPtr &account,
            const Tp::ConnectionPtr &connection,
            const QList<Tp::ChannelPtr> &channels,
            const QList<Tp::ChannelRequestPtr> &requestsSatisfied,
            const QDateTime &userActionTime,
            const Tp::AbstractClientHandler::HandlerInfo &handlerInfo);

private slots:
    void onCertVerifierReady(Tp::PendingOperation *op);
    void onCertVerifierFinished(Tp::PendingOperation *op);

private:
    QHash<Tp::PendingOperation *, Tp::MethodInvocationContextPtr<> > mVerifiers;
};

#endif

