/***************************************************************************
                          sasl-handler.h -  description
                             -------------------
    begin                : Saturday 7 April 2012
    copyright            : (C) 2011 by Andre Moreira Magalhaes
    copyright            : (C) 2011 by David Edmundson
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#ifndef SASLHANDLER_H
#define SASLHANDLER_H

#include <QObject>

#include <TelepathyQt/AbstractClientHandler>

namespace Tp
{
    class PendingOperation;
};

class SaslAuthOp;

class SaslHandler : public QObject, public Tp::AbstractClientHandler
{
    Q_OBJECT

public:
    explicit SaslHandler(const Tp::ChannelClassSpecList &channelFilter);

    bool bypassApproval() const;

    void handleChannels(const Tp::MethodInvocationContextPtr<> &context,
            const Tp::AccountPtr &account,
            const Tp::ConnectionPtr &connection,
            const QList<Tp::ChannelPtr> &channels,
            const QList<Tp::ChannelRequestPtr> &requestsSatisfied,
            const QDateTime &userActionTime,
            const Tp::AbstractClientHandler::HandlerInfo &handlerInfo);

private slots:
    void onAuthReady(Tp::PendingOperation *op);
    void onAuthFinished(Tp::PendingOperation *op);

private:
    QHash<Tp::PendingOperation *, Tp::MethodInvocationContextPtr<> > mAuthContexts;
};

#endif

