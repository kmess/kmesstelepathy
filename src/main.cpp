/***************************************************************************
                          main.cpp -  description
                             -------------------
    begin                : Wednesday 21 December 2011
    copyright            : (C) 2011 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <TelepathyQt/Debug>
#include <TelepathyQt/Types>
#include <TelepathyQt/AccountFactory>
#include <TelepathyQt/ChannelFactory>
#include <TelepathyQt/ClientRegistrar>
#include <TelepathyQt/ConnectionFactory>
#include <TelepathyQt/ContactFactory>
#include <TelepathyQt/ChannelClassSpec>

#include <QDebug>
#include <QtGui>

#include "roster-window.h"
#include "kmess-application.h"
#include "auth/sasl-handler.h"
#include "auth/tls-handler.h"
#include "utils/crashhandler.h"

int main(int argc, char **argv)
{
    // Initialize the crash handler first.
    if( argc > 0 )
    {
      CrashHandler::setAppName( QLatin1String( argv[0] ) );
    }

    CrashHandler::activate();

    Tp::registerTypes();

    // ugly way of enabling/disabling telepathy debug output
    if(argc > 1 && QString(argv[1])==(QString("--tpdebug")))
    {
      Tp::enableDebug(true);
      Tp::enableWarnings(true);
    }


    Tp::AccountFactoryPtr accountFactory = Tp::AccountFactory::create(QDBusConnection::sessionBus(),
                                                                      Tp::Account::FeatureCore);

    Tp::ConnectionFactoryPtr  connectionFactory = Tp::ConnectionFactory::create(
        QDBusConnection::sessionBus(),
        Tp::Features() << Tp::Connection::FeatureSelfContact
                       << Tp::Connection::FeatureCore
    );

    Tp::ChannelFactoryPtr channelFactory = Tp::ChannelFactory::create(QDBusConnection::sessionBus());
    channelFactory->addCommonFeatures(Tp::Channel::FeatureCore);

    Tp::Features textFeatures = Tp::Features() << Tp::TextChannel::FeatureMessageQueue
                                               << Tp::TextChannel::FeatureMessageSentSignal
                                               << Tp::TextChannel::FeatureChatState
                                               << Tp::TextChannel::FeatureMessageCapabilities;
    channelFactory->addFeaturesForTextChats(textFeatures);
    channelFactory->addFeaturesForTextChatrooms(textFeatures);

    Tp::ContactFactoryPtr contactFactory = Tp::ContactFactory::create(
        Tp::Features() << Tp::Contact::FeatureAlias
                       << Tp::Contact::FeatureAvatarToken
                       << Tp::Contact::FeatureAvatarData
                       << Tp::Contact::FeatureCapabilities
                       << Tp::Contact::FeatureSimplePresence
    );

    Tp::ClientRegistrarPtr registrar = Tp::ClientRegistrar::create(accountFactory, connectionFactory,
                                                                   channelFactory, contactFactory);

    Tp::SharedPtr<KMessApplication> app = Tp::SharedPtr<KMessApplication>(new KMessApplication(argc, argv));
    Tp::AbstractClientPtr handler = Tp::AbstractClientPtr(app);
    registrar->registerClient(handler, QLatin1String("KMess"));

    /*Auth handlers*/

    Tp::ChannelClassSpecList saslFilter;
    QVariantMap saslOtherProperties;
    saslOtherProperties.insert(
            TP_QT_IFACE_CHANNEL_TYPE_SERVER_AUTHENTICATION + ".AuthenticationMethod",
            TP_QT_IFACE_CHANNEL_INTERFACE_SASL_AUTHENTICATION);
    saslFilter.append(Tp::ChannelClassSpec(TP_QT_IFACE_CHANNEL_TYPE_SERVER_AUTHENTICATION,
                Tp::HandleTypeNone, false, saslOtherProperties));
    Tp::SharedPtr<SaslHandler> saslHandler = Tp::SharedPtr<SaslHandler>(new SaslHandler(saslFilter));
    if (!registrar->registerClient(
                Tp::AbstractClientPtr(saslHandler), QLatin1String("KMess.SASLHandler"))) {
        qWarning() << "Could not register SASL handler";
    }

    Tp::ChannelClassSpecList tlsFilter;
    tlsFilter.append(Tp::ChannelClassSpec(TP_QT_IFACE_CHANNEL_TYPE_SERVER_TLS_CONNECTION,
                Tp::HandleTypeNone, false));
    Tp::SharedPtr<TlsHandler> tlsHandler = Tp::SharedPtr<TlsHandler>(new TlsHandler(tlsFilter));
    if (!registrar->registerClient(
                Tp::AbstractClientPtr(tlsHandler), QLatin1String("KMess.TLSHandler"))) {
        qWarning() << "Could not register TLS handler";
    }

    return app.data()->exec();
}
