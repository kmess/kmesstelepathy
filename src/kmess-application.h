/***************************************************************************
                          kmess-application.h -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _KMESS_APP_H_
#define _KMESS_APP_H_

#include <QApplication>

#include <TelepathyQt/Account>
#include <TelepathyQt/AccountManager>
#include <TelepathyQt/AbstractClientHandler>
#include <TelepathyQt/SimpleTextObserver>

class QStandardItemModel;

class KMessApplication : public QApplication, public Tp::AbstractClientHandler
{
    Q_OBJECT

public:
    KMessApplication(int &argc, char **argv);
    int exec(void);

    /// Handles all incoming channel
    virtual void handleChannels(const Tp::MethodInvocationContextPtr<> & context,
            const Tp::AccountPtr &account,
            const Tp::ConnectionPtr &connection,
            const QList<Tp::ChannelPtr> &channels,
            const QList<Tp::ChannelRequestPtr> &channelRequests,
            const QDateTime &userActionTime,
            const Tp::AbstractClientHandler::HandlerInfo &handlerInfo);

    virtual bool bypassApproval() const;

    /// Connect with account (by account path)
    void connectAccount(QString accountPath);

    /// Connect with account (by account pointer)
    void connectAccount(Tp::AccountPtr account);

    /// Get accounts
    QStandardItemModel* accounts(){ return mAccounts; }

signals:
    void connectionStatusChanged(Tp::AccountPtr account, Tp::ConnectionStatus status);

private slots:
    void onAccountReady(Tp::PendingOperation *op);
    void onAccountOnline(Tp::PendingOperation *op);
    void onAccountManagerReady(Tp::PendingOperation *);
    void onConnectionChanged(Tp::ConnectionPtr conn);
    void onConnectionStatusChanged(Tp::ConnectionStatus status);

private:
    void isAccountReadyAndConnected();

    QString mAccountName;
    QStandardItemModel* mAccounts;

    /// Hashmap of open chats
    QHash<QString /*targetid*/, QWeakPointer<QObject> > mChats;

    Tp::SimpleTextObserverPtr mtextObserver;
    Tp::AccountPtr mAccount;
    Tp::AccountManagerPtr mAccountManager;
    Tp::ChannelFactoryPtr mChannelFactory;
    Tp::ConnectionFactoryPtr mConnectionFactory;
    Tp::ContactFactoryPtr mContactFactory;
    Tp::Features mAccountFeatures;
};

#endif
