/***************************************************************************
                          richtextparser.h -  description
                             -------------------
    begin                : Wednesday 1 February 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _RICH_TEXT_PARSER_H_
#define _RICH_TEXT_PARSER_H_

class QString;

class RichTextParser
{
public:
  enum Context
  {
    Default = 0,
    IncomingChatMessage = 1,
    OutgoingChatMessage = 2
  };
  /// Parses a rich text string into its html equivalent
  static QString parse(QString string, Context context);
};

#endif
