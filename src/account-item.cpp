/***************************************************************************
                          account-item.cpp -  description
                             -------------------
    begin                : Monday 9 April 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <TelepathyQt/Presence>

#include "account-item.h"
#include "status.h"

AccountItem::AccountItem(const Tp::AccountPtr &account, QStandardItemModel *parent)
    : QObject(parent),
      QStandardItem(/*parent*/),
      mAccount(account)
{
  connect(mAccount.data()
      , SIGNAL(displayNameChanged(const QString &))
      , SLOT  (onAccountUpdated()));
  connect(mAccount.data()
      , SIGNAL(currentPresenceChanged(const Tp::Presence &))
      , SLOT  (onAccountUpdated()));

  onAccountUpdated();
}

void AccountItem::onAccountUpdated()
{
  setIcon(Status::statusMap()->value(mAccount->currentPresence().status()));
  setText(mAccount->displayName());
}

AccountItem::~AccountItem()
{
}

QVariant AccountItem::data(int role) const
{
  switch(role)
  {
    case AccountItem::PathRole:
      return mAccount->objectPath();
    case AccountItem::AvatarDataRole:
      return mAccount->avatar().avatarData;
    default:
      return QStandardItem::data(role);
  }
}
