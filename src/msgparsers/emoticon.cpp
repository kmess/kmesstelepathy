/***************************************************************************
                          emoticon.cpp  -  description
                             -------------------
    begin                : Mon July 6 2012
    copyright            : (C) 2012 by Argyros Argyridis
    email                : arargyridis@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "emoticon.h"
/**
 * Constructor for custom emoticons, with custom picture path
 *
 * @param pictureName       The picture file name
 * @param tooltip           The name of this emoticon: it will be displayed as tooltip in the Emoticons Widget
 * @param pictureDirectory  The folder where this emoticon's image is located
 * @param isCustomEmoticon  Whether this is a custom or standard emoticon
 */
Emoticon::Emoticon( const QString &pictureName, const QString &tooltip, const QString &pictureDirectory, bool isCustomEmoticon )
    : pictureDirectory_(pictureDirectory)
    , pictureName_(pictureName)
    , height_(EMOTICONS_DEFAULT_SIZE)
    , isCustomEmoticon_(isCustomEmoticon)
    , originalPictureName_(pictureName)
    , tooltip_(tooltip)
    , valid_(false)
    , width_(EMOTICONS_DEFAULT_SIZE)
{
  // Try to find the location of the current image
  //update();
}

QStringList Emoticon::parse(QStringList &stringlist, bool enabled)
{
    if (enabled) {
	
        QHash<QString,QString> themeFileNames_;
        themeFileNames_[";)"]="/usr/share/emoticons/KMess-new/wink.png";
        themeFileNames_[":)"]="/usr/share/emoticons/KMess-new/smile.png";
        themeFileNames_[":P"]="/usr/share/emoticons/KMess-new/tongue.png";

        for (register int i = 0;  i< stringlist.size();  i++ ) {
            for (QHash<QString,QString>::Iterator hashIt = themeFileNames_.begin(); hashIt !=themeFileNames_.end(); hashIt++ ){
                //getting the position where the smile lies. Otherwise it's -1
                int position = stringlist.at(i).indexOf(hashIt.key(),0);
                if (position != -1) {
                    QStringList tempList = stringlist.at(i).split(hashIt.key(), QString::SkipEmptyParts);
                     stringlist[i] = tempList[0];
                     stringlist.insert(i+1,"<img src=\""+*hashIt+"\"/>");
                     if (tempList.size() > 1)
                     {
                         stringlist.insert(i+2,tempList[1]);
                     }
                }
            }
        }
    }
    return stringlist;
}
/**
 * Create an exact copy of another emoticon
 *
 * @param other  The object to copy
 */
Emoticon::Emoticon( const Emoticon &other )
  : pictureDirectory_(other.pictureDirectory_)
  , pictureName_(other.pictureName_)
  , picturePath_(other.picturePath_)
  , height_(EMOTICONS_DEFAULT_SIZE)
  , isCustomEmoticon_(other.isCustomEmoticon_)
  , originalPictureName_(other.originalPictureName_)
  , shortcuts_(other.shortcuts_)
  , tooltip_(other.tooltip_)
  , valid_(other.valid_)
  , width_(EMOTICONS_DEFAULT_SIZE)
{
}

/**
 * Destructor. Does nothing notable.
 */
Emoticon::~Emoticon()
{
}

/**
 * Add a text shortcut which will translate to this emoticon
 *
 * @param  shortcut   Shortcut text
 */
void Emoticon::addShortcut( const QString &shortcut )
{
  shortcuts_.append( shortcut );
}

/**
 * Get the picture file name
 */
const QString& Emoticon::getPictureName() const
{
  return pictureName_;
}


// Get the data hash for the picture file contents
const QString& Emoticon::getDataHash() const
{
  return dataHash_;
}
/**
 * Get the full path of the picture file
 */
const QString& Emoticon::getPicturePath() const
{
  return picturePath_;
}
/**
 * Get the first shortcut associated to this emoticon
 */
const QString& Emoticon::getShortcut() const
{
  return shortcuts_.first();
}

/**
 * Get all the shortcuts associated to this emoticon
 */
const QStringList & Emoticon::getShortcuts() const
{
  return shortcuts_;
}
/**
 * Delete all the shortcuts and replace them with a single one
 *
 * @param shortcut  The new shortcut
 */
void Emoticon::setShortcut( const QString &shortcut )
{
  shortcuts_.clear();
  shortcuts_.append( shortcut );
}

/**
 * Find if this emoticon really contains a valid picture
 */
bool Emoticon::isValid() const
{
  return valid_;
}

/**
 * Delete all the shortcuts and replace them with another list
 *
 * @param shortcuts  The new list of shortcuts
 */
void Emoticon::setShortcuts( const QStringList &shortcuts )

{
  shortcuts_.clear();
  shortcuts_ = shortcuts;
}
/**
 * Change the associated tooltip
 *
 * @param pictureTooltip  The new tooltip
 */
void Emoticon::setTooltip( const QString &pictureTooltip )
{
  tooltip_ = pictureTooltip;
}

/**
 * Get a suitable bit of HTML which represents the emoticon
 *
 * The images output as HTML are limited in width and height, though the original files keep their
 * original dimensions - and are sent untouched to your contacts.
 *
 * @param small   If true, the HTML will paint a tiny version of the emoticon
 */
const QString Emoticon::getHtml( bool small ) const
{
  int width, height;
  QString emoticonClass;

  // Enforce minimum and maximum size limits
  if( small )
  {
    width  = width_; // qMin( EMOTICONS_DEFAULT_SIZE, width_  ); // Require only a max height
    height = qMin( EMOTICONS_DEFAULT_SIZE, height_ );
  }
  else
  {
    width  = width_; // qMin( EMOTICONS_MAX_SIZE, width_  ); // Require only a max height
    height = qMin( EMOTICONS_MAX_SIZE, height_ );
  }

  // Resize the image so the aspect ratio is kept, and its dimensions don't exceed the enforced
  // maximum size
  if( width_ > height_ )
  {
      height = ( width * height_ ) / width_;
  }
  else
  {
      width = ( height * width_ ) / height_;
  }

  if( isCustomEmoticon_ )
  {
    emoticonClass = "customEmoticon";
  }
  else
  {
    emoticonClass = "standardEmoticon";
  }

  return "<img src='" + picturePath_ + "' alt='" + shortcuts_.first()
       + "' height='" + QString::number(height)
       + "' width='" + QString::number(width) + "' valign='bottom' class='"
       + emoticonClass + "' />";
}
