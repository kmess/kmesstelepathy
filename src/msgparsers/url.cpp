/***************************************************************************
                          url.cpp -  description
                             -------------------
    begin                : Sunday 29 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QDebug>
#include <QRegExp>
#include <QString>
#include <QStringList>

#include "url.h"

QStringList Url::parse(QStringList &stringlist, bool enabled)
{
  QStringList outputlist = QStringList();
  if(!enabled) return stringlist;
  qDebug() << "Url parser";
  foreach(QString string, stringlist)
  {
    outputlist.append(Url::parse(string));
  }
  return outputlist;
}

QStringList Url::parse(QString string)
{
    // TODO: place these regexps at the beginning of this file and
    // initialize them *once*!
    QRegExp linkRegExp;
    linkRegExp.setPattern( "\\b((?:http://|https://|ftp://|sftp://|www\\.)"
                           "\\S+)"
                                // match protocol string followed by the host/path
                           "[.,;!?]?(?:&lt;|\\s|$)"
                                // ending with <, \s or $, not counting .,;?!"' before
                                // (there are some more modifications to a matched
                                // URL below)
                         );
    linkRegExp.setMinimal(1);
    int index = linkRegExp.indexIn(string);
    QString replacement;
    if(index != -1)
    {
      QString matched = linkRegExp.cap( 1 );
      qDebug() << matched;

      // Some link normalizing: only allow ')' at the end if there's '(', same with ", etc
      if( matched.endsWith(")") && ! matched.contains("(") )
      {
        matched.chop( 1 );
      }
      if( matched.endsWith("&gt;") && ! matched.contains("&lt;") )
      {
        matched.chop( 4 );
      }
      if( matched.endsWith("}") && ! matched.contains("{") )
      {
        matched.chop( 1 );
      }
      if( matched.endsWith   ( "&#34;" )
          &&  matched.lastIndexOf( "&#34;", -6 ) == -1 )
      {
        matched.chop( 5 );
      }
      if( matched.endsWith   ( "&#39;" )
          &&  matched.lastIndexOf( "&#39;", -6 ) == -1 )
      {
        matched.chop( 5 );
      }

      // TODO wordwrap was removed compared to original KMess implementation

      if( matched.startsWith( "www." ) )
      {
        replacement = "<a href=\"http://" + matched + "\" target=\"_blank\">" +
          matched + "</a>";
      }
      else
      {
        replacement = "<a href=\"" + matched + "\" target=\"_blank\">" +
          matched + "</a>";
      }

      qDebug() << linkRegExp.cap(0) << replacement << linkRegExp.cap(2);
      int pos = linkRegExp.pos(1);
      return QStringList(string.left(pos) + replacement) << parse(string.mid(pos+matched.length()));
    }
    else
    {
      return QStringList(string);
    }
}
