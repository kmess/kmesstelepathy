/***************************************************************************
                          bbcode.h -  description
                             -------------------
    begin                : Wednesday 1 February 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

class QString;
class QStringList;

class BbCode
{
public:
  /// Surround links with <a href></a>
  static QStringList parse(QStringList &stringlist, bool enabled);
private:
  /// Parse the string
  static QStringList parse(QString string);
};


