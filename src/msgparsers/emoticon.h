#ifndef EMOTICON_H
#define EMOTICON_H

#include <QtCore/QStringList>
#include <QtCore/QHash>
/**
 * @brief Data class for an emoticon definition.
 *
 * This class represents an emoticon picture file along with the text shortcuts that translate into that picture.
 * Normal emoticon pictures are searched for in two folders: first in the one where is located the theme selected
 * by the user; then in the other where there's the default KMess theme. This way, if an emoticon is not present
 * in a theme that the user has chosen, the standard KMess emoticon is kept in its place.
 * A common emoticon which most themes don't have is the 'party' one, which in MSN is a smiley with a party hat
 * on it. You'll often see the KMess default version of this emoticon when you choose another theme.
 *
 * But the emoticons can also be created with a specific path where to look for the image file; a method useful for
 * custom emoticons. They don't have a default icon in the default KMess theme, so no defaults are searched for.
 *
 * For standard emoticons, file paths are updated by the EmoticonTheme class when the theme changes.
 *
 * Multiple emoticon shortcuts can be added to the emoticon with the addShortcut() method.
 * KMess supports for the emoticons all the image file types which are supported by KDE; but only some extensions
 * are looked for. If you want to add another image type for your emoticons, look at the updatePath() method.
 *
 *Copied from the old Kmess documentary
 * @author Argyros Argyridis, Mike K. Bennett
 * @ingroup Root
 */
#ifndef EMOTICONS_DEFAULT_SIZE
#define EMOTICONS_DEFAULT_SIZE    14
#endif

#ifndef EMOTICONS_MAX_SIZE
#define EMOTICONS_MAX_SIZE        128
#endif
class Emoticon
{
public:
    //default constructor
    Emoticon();
    //destructor
    virtual  ~Emoticon();
    // Constructor for custom emoticons
    Emoticon( const QString &pictureName, const QString &tooltip, const QString &pictureDirectory, bool isCustomEmoticon );
    //create an exact copy of the emoticon
    Emoticon(const Emoticon &other);

    //Add a valid shortcut for the emoticon
    void                            addShortcut(const QString& shortcut);

    //function that parses a message and extracts the emoticons from it
    static QStringList parse(QStringList &stringlist, bool enabled);
    // Get the data hash for the picture file contents (may be empty)
    const QString             &getDataHash() const;
    // Get a suitable bit of HTML which represents the emoticon
    const QString            getHtml( bool isSmall ) const;
    // Get the picture file name
    const QString            &getPictureName() const;
    // Get the full path of the picture file
    const QString            &getPicturePath() const;
    // Get the first shortcut associated to this emoticon
    const QString           &getShortcut() const;
    // Get all the shortcuts associated to this emoticon
    const QStringList     &getShortcuts() const;
    // Find if this emoticon really contains a valid picture
    bool                            isValid() const;
    // Change the name of the emoticon picture
    //void                           setPictureName( const QString &pictureName, const QString &pictureDirectory );
    // Delete all the shortcuts and replace them with a single one
    void                           setShortcut( const QString &shortcut );
    // Delete all the shortcuts and replace them with another list
    void                           setShortcuts( const QStringList &shortcuts );
    // Change the associated tooltip
    void                     setTooltip( const QString &pictureTooltip );
    // Update the internal emoticon data
    void                           update();




public: //public static methods
    // Return whether a string is suitable for use as an emoticon shortcut
    static bool                 isValidShortcut( const QString &shortcut );

private: //private class attributes
    //directory of the custom emoticon image. Unused for standard emoticons
    QString                      pictureDirectory_;
    //file name of the emoticon
    QString                      pictureName_;
    //
    QString                      picturePath_;
    //The hash of this emoticon (sha1d). Used to compare with other emoticons
    QString                      dataHash_;
    //Height of the emoticon
    int                      height_;
    //Width of the emoticon
    int                      width_;
    // Whether this Emoticon represents a standard KMess emoticon or a custom (personal) one
    bool                            isCustomEmoticon_;
    // The file name of the initial emoticon image
    QString                     originalPictureName_;
    // The list of shortcuts that represent the emoticon, eg ":-)" and ":)"
    QStringList               shortcuts_;
    // A name for the emoticon, eg "Happy"
    QString                      tooltip_;
    // Does the image exist and can it be used?
    bool                            valid_;





};

#endif // EMOTICON_H
