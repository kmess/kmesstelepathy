/***************************************************************************
                          bbcode.cpp -  description
                             -------------------
    begin                : Wednesday 1 February 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <QDebug>
#include <QRegExp>
#include <QString>
#include <QStringList>

#include "bbcode.h"

QStringList BbCode::parse(QStringList &stringlist, bool enabled)
{
  QStringList outputlist = QStringList();

  if(enabled == false)
  {
    // When disabled remove all known tags
    foreach(QString string, stringlist)
    {
      string
        .replace(  "[b]", "", Qt::CaseInsensitive )
        .replace( "[/b]", "", Qt::CaseInsensitive )
        .replace(  "[i]", "", Qt::CaseInsensitive )
        .replace( "[/i]", "", Qt::CaseInsensitive )
        .replace(  "[u]", "", Qt::CaseInsensitive )
        .replace( "[/u]", "", Qt::CaseInsensitive )
        .replace(  "[s]", "", Qt::CaseInsensitive )
        .replace( "[/s]", "", Qt::CaseInsensitive );

      string.replace( QRegExp( "\\[/?(c|a)(=#?[0-9a-z,]+)?\\]", Qt::CaseInsensitive ), "" );

      outputlist.append(string);
    }
  }
  else
  {
    foreach(QString string, stringlist)
    {
      string
        .replace(  "[b]", "<b>", Qt::CaseInsensitive )
        .replace( "[/b]", "</b>", Qt::CaseInsensitive )
        .replace(  "[i]", "<i>", Qt::CaseInsensitive )
        .replace( "[/i]", "</i>", Qt::CaseInsensitive )
        .replace(  "[u]", "<u>", Qt::CaseInsensitive )
        .replace( "[/u]", "</u>", Qt::CaseInsensitive )
        .replace(  "[s]", "<s>", Qt::CaseInsensitive )
        .replace( "[/s]", "</s>", Qt::CaseInsensitive );

      // remove color codes for now
      string.replace( QRegExp( "\\[/?(c|a)(=#?[0-9a-z,]+)?\\]", Qt::CaseInsensitive ), "" );
      outputlist.append(string);
    }
  }
  return outputlist;
}

QStringList BbCode::parse(QString string)
{
}
