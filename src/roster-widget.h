/***************************************************************************
                          roster-widget.h -  description
                             -------------------
    begin                : Sunday 22 January 2012
    copyright            : (C) 2012 by Ruben Vandamme
    email                : ruben@kmess
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _KMESS_ROSTER_WIDGET_H_
#define _KMESS_ROSTER_WIDGET_H_

#include "src/ui_contactlist.h"

#include <QWidget>

#include <TelepathyQt/Contact>
#include <TelepathyQt/Connection>



namespace Tp {
class Connection;
class PendingOperation;
}

class QAction;
class QDialog;
class QLineEdit;
class QMenu;
class QPushButton;
class QStandardItemModel;
class QTreeView;
class RosterFilter;
class RosterGroupFilter;
class RosterItem;

class RosterWidget : public QWidget
{
    Q_OBJECT

public:
    RosterWidget(Tp::AccountPtr account, QWidget *parent = 0);
    virtual ~RosterWidget();

    Tp::ConnectionPtr connection() const { return mConn; }
    //void setConnection(const Tp::ConnectionPtr &conn);
    void setConnection(Tp::AccountPtr account/*const ConnectionPtr &conn*/);
    void unsetConnection();

    QStandardItemModel *contactList() const { return mList2; }

protected:
    virtual RosterItem *createItemForContact(
            const Tp::ContactPtr &contact,
            bool &exists);
    virtual void updateActions(RosterItem *item) { }

private slots:
    void onContactManagerStateChanged(Tp::ContactListState state);
    void onPresencePublicationRequested(const Tp::Contacts &);
    void onItemSelectionChanged();
    void onAddButtonClicked();
    void onAuthActionTriggered(bool);
    void onDenyActionTriggered(bool);
    void onRemoveActionTriggered(bool);
    void onBlockActionTriggered(bool);
    void onContactRetrieved(Tp::PendingOperation *op);
    void updateActions();
    void onContactListItemClicked(const QModelIndex &index);
    void onAvatarChanged(const Tp::Avatar &avatar);
    void searchContact(const QString &searchFor);
    void onCurrentPresenceChanged(const Tp::Presence &presence);
    /// A contactlist item was changed
    void onRosterItemChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    /// When a sorting is updated, updated view accordingly
    void onGroupLayoutChanged();
    /// When a group is collapsed, update model accordingly
    void onGroupCollapsed(const QModelIndex & index);
    /// When a group is expanded, update model accordingly
    void onGroupExpanded(const QModelIndex & index);

    /// Show or hide contact search
    void onToggleSearch();
    /// Show or hide offline contacts
    void onToggleOfflineContacts();
    /// Show or hide empty groups
    void onToggleEmptyGroups();

private:
    void createActions();
    void setupGui();

    Tp::ConnectionPtr mConn;
    Tp::AccountPtr mAccount;
    QAction *mAuthAction;
    QAction *mRemoveAction;
    QAction *mDenyAction;
    QAction *mBlockAction;

    ///Default contactlist
    QStandardItemModel *mList2;
    ///Filters contacts
    RosterFilter *mFilteredList;
    ///Filters empty groups on request
    RosterGroupFilter *mGroupFilteredList;

    QDialog *mAddDlg;
    QLineEdit *mAddDlgEdt;
    QMenu *mStatusMenu;

    Ui::mContactListView ui;
    ///Toggles search specific ui behaviour (group expanding, etc)
    bool mSearchEnabled;

    ///Search contact action
    QAction *mSearchContactAction;
    ///Add contact action
    QAction *mAddContactAction;
    ///Show offline contacts action
    QAction *mOfflineContactAction;
    ///Show empty groups action
    QAction *mEmptyGroupsAction;

    QSettings *mSettings;
};

#endif
