#include <QDebug>
#include <QDesktopServices>
#include <QIcon>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QWidget>

#include <TelepathyQt/AvatarData>
#include <TelepathyQt/Contact>
#include <TelepathyQt/Presence>

#include "status.h"
#include "roster-delegate.h"
#include "roster-filter.h"
#include "roster-groupfilter.h"
#include "roster-item.h"

/// Default spacing between items
#define ITEM_SPACE    4


RosterDelegate::RosterDelegate( QTreeView *parent, QStandardItemModel *list, RosterFilter *filteredList, RosterGroupFilter *groupFilteredList )
    : QStyledItemDelegate( parent ),
      mTreeView(parent),
      mList(list),
      mFilteredList(filteredList),
      mGroupFilteredList(groupFilteredList)
{
  mGroupFont.setBold(true);

  mArrowCollapsed = QIcon::fromTheme("arrow-right");
  mArrowExpanded = QIcon::fromTheme("arrow-down");
  mBlocked = QIcon::fromTheme("dialog-cancel");

  mStatusIcons = Status::statusMap();
}

RosterDelegate::~RosterDelegate()
{
}

void RosterDelegate::paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
  // Only render column zero
  if( index.column() != 0 )
  {
    return;
  }

  // Save the painter state to avoid messing up further painting operations with it
  painter->save();

  // Reset coordinate translation in the painter (defaults are relative to the parent's window) - and make rendering prettier possibly
  painter->translate( 0, 0 );
  painter->setRenderHints( QPainter::Antialiasing | QPainter::SmoothPixmapTransform |
                           QPainter::TextAntialiasing, true );


  // Compute the positions where the objects will be painted
  QPoint originPoint;
  originPoint = option.rect.topLeft();
  originPoint.rx()++;
  originPoint.ry()++;

  if(index.data(RosterItem::TypeRole).toInt() == RosterItem::ContactType)
  {
    QString avatarFile(index.data(RosterItem::AvatarFileRole).toString());
    if(avatarFile.isEmpty())
    {
      avatarFile=QString(QCoreApplication::applicationDirPath() + "/../share/apps/kmess-telepathy/pics/unknown.png");
    }
    QIcon avatar(avatarFile);
    avatar.paint(painter,originPoint.x()+20,originPoint.y(),48,48);
    if(!index.data(RosterItem::BlockedRole).toBool())
    {
        mStatusIcons->value(index.data(RosterItem::PresenceStatusRole).toString()).paint(painter,originPoint.x()+20,originPoint.y()+32,16,16);
    }
    else
    {
	mBlocked.paint(painter,originPoint.x()+20,originPoint.y()+32,16,16);
    }
    painter->drawText(originPoint.x()+20+48+ITEM_SPACE,originPoint.y()+14,index.data(Qt::DisplayRole).toString());
    painter->drawText(originPoint.x()+20+48+ITEM_SPACE,originPoint.y()+40,index.data(RosterItem::PresenceStatusMessageRole).toString());
  }
  else
  {
    (mTreeView->isExpanded(index)?mArrowExpanded:mArrowCollapsed).paint(painter,originPoint.x(),originPoint.y(),16,16);
    painter->setFont(mGroupFont);
    painter->drawText(originPoint.x()+16+ITEM_SPACE,originPoint.y()+14,
        QString("%1 (%2/%3)")
        .arg(index.data(Qt::DisplayRole).toString())
        .arg(index.data(RosterItem::ContactsOnlineRole).toInt())
        .arg(index.data(RosterItem::ContactsRole).toInt()));
  }

  // Restore the painter to the previous state
  painter->restore();
}

QSize RosterDelegate::sizeHint( const QStyleOptionViewItem &option, const QModelIndex &index ) const
{
  QSize defaultsize = QStyledItemDelegate::sizeHint(option, index);

  if(index.data(RosterItem::TypeRole).toInt() == RosterItem::ContactType)
  {
    return QSize(defaultsize.width(), 48+2);
  }
  else
  {
    return QSize(defaultsize.width(), 16+2);
  }
}
