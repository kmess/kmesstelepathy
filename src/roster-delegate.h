#ifndef _ROSTER_DELEGATE_H_
#define _ROSTER_DELEGATE_H_

#include <QAction>
#include <QModelIndex>
#include <QPainter>
#include <QStyledItemDelegate>
#include <QStyleOptionViewItem>
#include <QTextStream>
#include <QTreeView>

class RosterFilter;
class RosterGroupFilter;
class QStandardItemModel;

class RosterDelegate : public QStyledItemDelegate
{
    Q_OBJECT

  public:

    // Constructor
    RosterDelegate( QTreeView *parent, QStandardItemModel *list, RosterFilter *filteredList, RosterGroupFilter *groupFilteredList );
    // Destructor
    ~RosterDelegate();


  public:

    /// Paint an item of the contact list
    void            paint( QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index ) const;
    /// Return an hint about the size of an item
    QSize           sizeHint( const QStyleOptionViewItem &option, const QModelIndex &index ) const;
  private:
    QTreeView *mTreeView;

    /// Icons we'll need during rendering
    QIcon mArrowCollapsed;
    QIcon mArrowExpanded;
    QIcon mBlocked;
    QMap<QString, QIcon> *mStatusIcons;
    /// Full contactlist
    QStandardItemModel *mList;
    /// Filtered contactlist/proxymodel for contact
    RosterFilter *mFilteredList;
    /// Filtered contactlist/proxymodel for groups
    RosterGroupFilter *mGroupFilteredList;
    /// Font used for groups
    QFont mGroupFont;
};

#endif
